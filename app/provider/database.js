import {Injectable} from 'ionic-angular';

export class Database {

	constructor(){
		this.database = new PouchDB("demo",{adapter:'websql'});
	}
	
	get(documentId){
		return this.database.get(documentId).then((doc)=>{
			return doc;
		}).catch((err)=>{
			return {error : true, err : err};
		});
	}

	default(){

		let document = {
			_id : "demo",
			username : "samparsky",
			firstname : "samuel",
			lastname : "omidiora",
			facebook_id : "",
			email : "samaparsky@gmail.com",
			status : "1", //pending or active
			notes : [],
			created_at : new Date().toISOString(),
			glass_credit : "",
			glass_coins :"",
			glass_points:"",
			expire : ""
		};

		this.save(document);
	}
	
	save(document){
		console.log("document"+document);
		if(!document._id){
			this.database.post(document).then(function(response) {
				console.log(response);
			}).catch(function(error) {
				console.log(error);
			})
		} else {
			this.database.put(document).then(function(response) {
				console.log(response);
			}).catch(function(error){
				console.log(error);
			})
		}
	}

	createNote(doc,key){
		
	}

	startListening() {

	}
}