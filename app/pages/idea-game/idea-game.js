import {Page, NavController,NavParams,Modal,Alert,Platform} from 'ionic-angular';
import {SuccessExtractFacts} from '../modal/success-extract-facts/success-extract-facts';
import {DatabaseService} from '../../providers/database/database';
import {UserService} from '../../providers/user/user';
import {TabsPage} from '../tabs/tabs';

@Page({
  templateUrl: 'build/pages/idea-game/idea-game.html',
  providers: [DatabaseService,UserService]
})
export class IdeaGamePage {
  static get parameters() {
    return [[NavController],[NavParams],[Platform],[DatabaseService],[UserService]];
  }

  constructor(nav,params,platform, dbService, UserService) {
    
    this.note = params.get('note');
    this.edit = params.get('edit') || false;
    this.position = params.get('position');

    this.apples = [0];
    this.glass_coin = 1;
    this.colors = ['#388E3C','#ffa000','#9c27b0','#03a9f4','#E91e63','#303F9F','#512DA8','#E64A19'];
    this.color_index = 0;
    this.facts = [];
    if(this.note.facts){
      this.facts = this.note.facts;
    } else {
      this.facts.push({idea : "", answer : "", color: "#FBC02D"});
    }

    this.nav = nav;
    this.platform = platform;

    this.dbService = dbService;
    this.UserService = UserService;
    

    // window.plugins.NativeAudio.preloadSimple('battery','build/assets/audio/battery.wav',function(msg){},function(msg){
    //   console.log("audio errror"+msg);
    // });
      // this.sound = new Media("build/assets/audio/coinsgain.ogg",function(){
      //   console.log("Successfully loaded file");
      // });
      //this.sound.setVolume(1);
  }

  getColor(){
    
  }

  seedNumber(){
    return Math.floor((Math.random() * 8 ) + 0);
  }

  error() {
      let  alert = Alert.create({ 
      title: 'Sorry', 
      subTitle: 'You have to input more than five characters in the field', 
      buttons: ['Ok'] 
    }); 

    this.nav.present(alert);
  }

  maximum_error() {
      let  alert = Alert.create({ 
      title: 'Sorry', 
      subTitle: 'You are not allowed to input more than 50 characters in the field', 
      buttons: ['Ok'] 
    }); 

    this.nav.present(alert);
  }

  changeColor(){
    console.log(this.color_index++)
    if(this.color_index > this.colors.length){
      return this.color_index;
    }
  }
  
  addFact() {
    this.changeColor();
    let length = this.apples.length;

    let facts_length = this.facts.length;
    let current_idea = this.facts[facts_length - 1].idea;
    
    if(current_idea == "" ||  current_idea.length < 5){
      return this.error();
    }

    if(current_idea.length > 50){
      return this.maximum_error();
    }

    if(length < 10){
      let apple_lastItem = parseInt(this.apples[this.apples.length - 1]);
      let apple_value = apple_lastItem + 1;
      this.glass_coin += 1;
      //this.apples.push(apple_value);
      console.log("apple" + this.apples);  
    } else {
      this.apples.splice(0);
      //this.glass_coin += 10;
    }
    
    console.log("hi");
    console.log(this.facts);
    var seedNumber = this.seedNumber();
    console.log("seedNumber");
    console.log(seedNumber);    
    var color = this.colors[seedNumber];
    this.facts.push({idea : "", answer: "","color": color});
    //window.plugins.NativeAudio.play( 'battery' );

    // if(this.platform.is('android')){
     //this.sound.play();      
    // }
  }

  removeFact(index){
    let apple_lastItem = this.apples.length - 1;
    this.apples.splice(apple_lastItem);
    console.log("remove apples"+this.apples);
    this.glass_coin -= 1;
    console.log(index);
    var lastItem = this.facts.length - 1;
    this.facts.splice(index,1);
  }

  home(){
    console.log("in home area");
    this.dbService.get('demo').then((response) => {
      console.log("getting home response");
      this.note.facts = this.facts;
      console.log("nots in home"+JSON.stringify(this.note));
        var data = response;
        data.notes.push(this.note);
        console.log("create");
        this.dbService.save(data);
        this.UserService.init();
        //this.viewCtrl.dismiss();
      this.nav.setRoot(TabsPage);
      this.nav.push(TabsPage);
      }).catch((error)=> {
        let alert = Alert.create({
          title: 'Sorry!',
          subTitle: 'Could not save note. Please try again.',
          buttons: ['OK']
        });
      });
  }
  
  save(){
    if(this.facts.length < 2) {
        let  alert = Alert.create({ 
          title: 'Sorry', 
          subTitle: 'You have to create at least two ideas!', 
          buttons: ['Ok'] 
        }); 
         this .nav.present(alert);
         return;
    }

    console.log("saving extracting facts");
    console.log(this.facts);
    this.note['facts'] = this.facts;
    this.note.state = 'facts';
    let success = Modal.create(SuccessExtractFacts,{'note':this.note,edit : this.edit, position : this.position});
    this.nav.present(success);
    //console.log(this.note);
  }

}
