import {NavController,Page,Loading} from 'ionic-angular';
import {ServerService} from '../../providers/server/server';
import {UserService} from '../../providers/user/user';
import {Connectivity} from '../../providers/connectivity/connectivity';

@Page({
  templateUrl: 'build/pages/page3/page3.html',
  providers: [ServerService,UserService,Connectivity]
})

export class Page3 {
  
  static get parameters(){
    return[[NavController],[ServerService],[UserService],[Connectivity]];
  }

  constructor(nav,server,userservice,connectivity) {
    this.nav = nav;
  	this.loading = "";
    this.server = server;
    this.connectivity = connectivity;
    this.userservice = userservice;
    this.mydata = userservice.getData();
    this.mypoint = "";
    this.loaded = false;
    this.message = "loading...";
    this.percent = "";
  }

  onPageWillEnter(){
    console.log("this week");
    if(this.connectivity.isOnline()){
      new Promise(resolve => {
        this.getData();  
      });
    }
  }

  calculatePercentage(goal, points){
    console.log("calculatePercentage");
    console.log(goal);
    console.log(points);
    
    var percent = Math.round((points / goal) * 100);
    console.log("percent");
    console.log(percent);
    this.percent = percent;
  }

  getData(){
      var facebook_id = this.mydata;
      console.log('facebook_id');
      console.log(facebook_id);
      this.server.getSync('leaderboard/'+this.mydata.facebook_id, (result)=> {
        console.log("result");
        console.log(result);
      
        this.data = result.data;
        this.total = this.data.total_points[0];
        this.goal  = this.data.goal;
        this.goal_created_at = this.goal.created_at;
        this.leader = this.data.leaders;

        this.current_user = this.data.current_user;

        if(!this.current_user.points){
          this.current_user.points = 0;
        }
        console.log("this.current_user");
        console.log(this.current_user);

        this.message = "";
        this.loaded = true;
                
        this.calculatePercentage(this.goal.point,this.total.points);
      });
  }

  calculatePoints(goal){
    this.userservice.points();
  }
  
}
