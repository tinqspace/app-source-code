export class PlayGameState extends Phaser.State {

	constructor(){
		super();
		// this.sga = jup;
		// this.s = bbat;
		//alert(jup);
		//alert(bbat);
	}

	init(){
		this.data = JSON.parse(window.localStorage.getItem('userdata'));
		this.note_index = parseInt(window.localStorage.getItem('note_index'));
		this.notes = this.data.notes[this.note_index];
		console.log("init");
		console.log(this.notes);
		this.level = this.notes.level;
	}

	preload(){

		this.game.load.image('background','build/assets/image/deep-space.jpg');
    	this.game.load.image('play_button','build/assets/image/g1032.png');
    	this.game.load.image('background','build/assets/image/deep-space.jpg');
	    this.game.load.image('play_button','build/assets/image/g1032.png');
	    this.game.load.image('button_background','build/assets/image/version-button.png');
	    this.game.load.image('percent_image','build/assets/image/percentage.png');
	    this.game.load.image('correct_image','build/assets/image/correct.png');
	    this.game.load.image('wrong_image','build/assets/image/wrong.png');
	    this.game.load.image('timer_image','build/assets/image/timer.png');

	    // //level image assets
	   
	    this.game.load.image('glass_coin','build/assets/image/glasscoin.png');
	    this.game.load.spritesheet('button', 'build/assets/image/button_sprite_sheet.png', 193, 71);
	    //audio files
	    //this.game.load.audio('play_audio','build/assets/audio/battery.wav');
	    this.game.load.audio('click_audio','build/assets/audio/tapword.ogg');
	    this.game.load.audio('coin_gain','build/assets/audio/coinsgain.ogg');
	    this.game.load.audio('congratulation','build/assets/audio/congratulations.ogg');
	    this.game.load.audio('correctanswer','build/assets/audio/correctanswer.ogg');
	    this.game.load.audio('failure','build/assets/audio/failure.ogg');
	    this.game.load.audio('gamebutton','build/assets/audio/gamebuttons.ogg');
	    this.game.load.audio('gameover','build/assets/audio/gameover.ogg');
	    this.game.load.audio('gamestart','build/assets/audio/gamestartnew.ogg');
	    this.game.load.audio('glasscoinincrease','build/assets/audio/glasscoinincrease.ogg');
	    this.game.load.audio('greatejob','build/assets/audio/greatejob.ogg');
	    this.game.load.audio('wronganswer','build/assets/audio/wronganswer.ogg');


	    //level images
	    this.game.load.image('preschooler','build/assets/image/1preschooler.png');
	    this.game.load.image('middleschooler','build/assets/image/2middleschooler.png');
	    this.game.load.image('junior','build/assets/image/3junior.png');
	    this.game.load.image('senior','build/assets/image/4senior.png');
	    this.game.load.image('freshman','build/assets/image/5freshman.png');
	    this.game.load.image('sophomore','build/assets/image/6sophomore.png');
	    this.game.load.image('graduate','build/assets/image/7graduate.png');
	    this.game.load.image('masters','build/assets/image/8masters.png');
	    this.game.load.image('doctor','build/assets/image/9doctor.png');
	    this.game.load.image('prof','build/assets/image/10prof.png');

	    this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    	this.scale.updateLayout();

	}

	create(){
	    var scaleRatio = window.devicePixelRatio/1.5;

	    var textScaleRatio = window.devicePixelRatio / 1.2;

		console.log("PlayGameState");
    	var background  = this.game.add.tileSprite(0 , 0,this.game.width , this.game.height, 'background');
    	var game_title  = this.game.add.text(this.game.world.centerX - 240,this.game.world.centerY - 300, 'Knowledge Reinforcement',{fill : "#ffffff","font": "30px Arial"});
	    game_title.stroke = "#FF4081";
	    game_title.strokeThickness = 10;
	    game_title.setShadow(2, 10, "#303F9F", 20, true, false);
	    game_title.scale.setTo(scaleRatio,scaleRatio);

    	this.click_audio =  this.game.add.audio('gamebutton');
    	this.game_start =  this.game.add.audio('gamestart');
    	this.game_start.play();

    	// this.gamestart = this.game.add.audio('gamestart');
    	// this.gamestart.play();
    	var demo_level = this.level + 1;
	    var note_title    = this.game.add.text(this.game.world.centerX - 120, this.game.world.centerY - 200 , this.title,{fill : "#ffffff","font": "24px Arial"});
    	note_title.anchor.set(0.5);
	    
	    var note_topic    = this.game.add.text(this.game.world.centerX - 50 , this.game.world.centerY - 170,this.topic, {fill: "#ffffff","font": "24px Arial"});
	    note_topic.scale.setTo(scaleRatio,scaleRatio);
	    var current_level = this.game.add.text(this.game.world.centerX - 80 , this.game.world.centerY - 180,"LEVEL "+demo_level,{fill : "#ffffff","font": "32px Arial"});
	    current_level.scale.setTo(scaleRatio,scaleRatio);

	    var pre_game_title = this.game.add.text(this.game.world.centerX - 250, this.game.world.centerY - 120, "You need a minimum score of",{fill: "#ffffff", "font": "30px Arial"});
	    pre_game_title.scale.setTo(scaleRatio,scaleRatio);
	    var required_score = this.game.add.text(this.game.world.centerX - 50, this.game.world.centerY - 60, this.calculateRequiredPercentage(this.level)+"%", {fill: "#ffffff", "font": "32px Arial"});
	    required_score.scale.setTo(scaleRatio,scaleRatio);
	    var complete_level = this.game.add.text(this.game.world.centerX - 200, this.game.world.centerY, "to complete this level",{fill: "#ffffff", "font": "32px Arial","wordWrap":true,wordWrapWidth: this.game.width,"align":"center"});
	    complete_level.scale.setTo(scaleRatio,scaleRatio);
	    var timer_title = this.game.add.text(this.game.world.centerX - 40, this.game.world.centerY+60, "Time:",{fill: "#ffffff", "font": "30px Arial"});
	    timer_title.scale.setTo(scaleRatio,scaleRatio);
	    var timer_image    = this.game.add.sprite(this.game.world.centerX - 100 ,this.game.world.centerY + 120,'timer_image');
	    timer_image.scale.setTo(scaleRatio,scaleRatio);
	    var time_score = this.game.add.text(this.game.world.centerX - 40, this.game.world.centerY + 120, this.calculateTimePerLevel(this.level) + " SECS",{fill: "#ffffff", "font": "30px Arial"});
	    time_score.scale.setTo(scaleRatio,scaleRatio);
	    var play_button    = this.game.add.button(this.game.world.centerX - 250 ,this.game.world.centerY + 180,'play_button',this.play,this , 2 , 1, 0);
	    play_button.width = 220;
	    play_button.height = 80;
	    play_button.scale.setTo(scaleRatio, scaleRatio);  
	}	

	calculateRequiredPercentage(level){
	   	var required = 55 + ((level) * 5);
	    return required;
  	}

  	calculateTimePerLevel(level){
		return 55 - ( 5 * level);
		//return 5;
	}

  	play(){
  		this.game_start.stop();
    	this.click_audio.play();
  		this.game.state.start('maingame',true,false,this.data,this.note_index,0, 5 , 0);
  	}
}