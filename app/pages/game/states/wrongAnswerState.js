export class WrongAnswerState extends Phaser.State {

	constructor(){
		super();
		this.scaleRatio = window.devicePixelRatio / 1.2;

	}

	init(data,note_index,index,glass_coins, percentage, user_answer){
		console.log("percentage");
		console.log(percentage);
		this.data = data;
		this.note_index = note_index,
		this.notes = this.data.notes[this.note_index];
		console.log("right");
		console.log(this.data);
		console.log(this.note_index);
		console.log(this.notes);
		this.level = this.notes.level;

		this.fact_index = index;
		this.glass_coin = glass_coins;
		this.percentage = percentage;
		this.user_answer = user_answer;
		this.right_answer = this.data.notes[this.note_index].facts[index - 1].answer;
		console.log("the right answer");
		console.log(this.right_answer);
	}

	preload(){
		
	}
	
	create(){
    
    	var background  = this.game.add.tileSprite(0 , 0,this.game.width , this.game.height, 'background');	 
    	var wrong_background = this.game.add.sprite(this.game.world.centerX - 180, this.game.world.centerY - 360 ,'wrong_image');
    	wrong_background.scale.setTo(this.scaleRatio, this.scaleRatio);

	    var wrong_text = this.game.add.text(this.game.world.centerX-30, this.game.world.centerY - 200, "Next",{fill: "#ffffff","font":"24px Arial"});
	    wrong_text.inputEnabled = true;
	    wrong_text.padding.set(0,10);
	    wrong_text.events.onInputDown.add(this.next, this);
	    wrong_text.scale.setTo(this.scaleRatio,this.scaleRatio);

    	var your_answer_title = this.game.add.text(this.game.world.centerX-50, this.game.world.centerY - 50, "Your Answer",{fill: "#ffffff","font":"24px Arial"});
    	your_answer_title.scale.setTo(this.scaleRatio, this.scaleRatio);

    	var your_wrong_answer = this.game.add.text(10, this.game.world.centerY,this.user_answer,{fill:"#000000",align: "center", backgroundColor: "#ffffff", wordWrap: true, wordWrapWidth: window.innerWidth});
    	your_wrong_answer.scale.setTo(this.scaleRatio, this.scaleRatio);

    	this.click_audio =  this.game.add.audio('gamebutton');

    	var answer_title = this.game.add.text(this.game.world.centerX-30, this.game.world.centerY + 80, "Answer",{fill: "#ffffff","font":"24px Arial"});
    	answer_title.scale.setTo(this.scaleRatio, this.scaleRatio);
    	
    	var r_answer = this.game.add.text(10, this.game.world.centerY + 220 ,this.right_answer,{fill:"#000000",align: "center", backgroundColor: "#ffffff", wordWrap: true, wordWrapWidth: window.innerWidth});
    	r_answer.scale.setTo(this.scaleRatio, this.scaleRatio);

	}

	next(item) {
		this.click_audio.play();
		if(this.glass_coin == 0){
			this.game.state.start('gameover',true,false,this.data,this.note_index,this.calculateRequiredPercentage(this.level), this.glass_coin);
			return;
		}

		if(this.fact_index == this.notes.facts.length) {
			if(this.percentage >= this.calculateRequiredPercentage(this.level) || this.glass_coin == 0){
				this.game.state.start('finished',true,false,this.data,this.level,this.note_index,this.fact_index,this.calculateRequiredPercentage(this.level), this.glass_coin);
			} else {
				this.game.state.start('gameover',true,false,this.data,this.note_index,this.calculateRequiredPercentage(this.level),this.glass_coin);
			}
			//go to game over page or success page depending on if percentage score for level met
			//alert("next");
		} else {
			this.game.state.start('maingame',true,false,this.data,this.note_index,this.percentage ,this.glass_coin, this.fact_index);
		}
	}

	calculateRequiredPercentage(level){
	   	if(level == 0){
			return 55;
		}
	   	var required = 55 + ((level - 1) * 5);
	    return required;
  	}	

}