import {Button} from '../objects/buttonObject';
import {Life} from '../objects/lifeObject';

export class MainGameState extends Phaser.State {
	constructor(){
		super();
		this.question = "";
		this.lifesRemaining = [];
		this.score = 0;
		this.glass_coins = 0;
		this.timer  = 0;
		this.answer = [];
		this.answer_initial_position = {};
		this.idea_length = 0;
		this.widthStore = 0;
		this.heightStore = 0;

		this.scaleRatio = window.devicePixelRatio/1.2;

		this.images = ["pending.png","1preschooler.png","2middleschooler.png",
		    "3junior.png","4senior.png","5freshman.png","6sophomore.png","7graduate.png",
		    "8masters.png","9doctor.png","10prof.png"];
	}

	init(data, note_index,percentage,glass_coins,fact_index){
		this.data = data;
		this.note_index = note_index;
		this.notes = this.data.notes[this.note_index];
		this.level = this.notes.level;
		this.idea = this.notes.facts[fact_index].answer;
		this.fact = this.notes.facts[fact_index].idea;
		this.index = fact_index;
		this.glass_coins = glass_coins;
		this.percentage = percentage;
		console.log("percentage");
		console.log(this.percentage);
	}

	preload(){

	}

	addScore(){
		var margin_x = this.game.width - 220;
	    var percentSprite = this.game.add.sprite(margin_x, 10, 'percent_image');
	    percentSprite.width = 100;
	    percentSprite.height = 50;
	    this.percentageScore =  this.game.add.text(margin_x + 20, 12,this.percentage+"%",{fill : "#c4884b","font": "32px Arial",wordWrap: true, wordWrapWidth: percentSprite.width});
	    this.percentageScore.stroke = "#654321"
    	this.percentageScore.strokeThickness = 6;
	}

	calculateTimePerLevel(level){
		return 55 - ( 5 * level)
		//return 5;
	}

	addTimer(){
		var margin_x = this.game.width - 50;
	    var margin_y = 20;
	    var timer_image = this.game.add.sprite(margin_x - 40,margin_y - 10,'timer_image');
	    this.myGameTimer = this.game.add.text(margin_x,margin_y,this.timer,{fill: "#ffffff"});
	    
	    //creating time event to update timer
	    this.time = this.game.time.create(false);
	    this.time.loop(1000, this.updateTimer, this);
	    this.time.start();
	}

	updateTimer(){
		this.timer += 1;

		if(this.timer == this.calculateTimePerLevel(this.level)){
			this.checkAnswer();
		}

	    this.myGameTimer.text = this.timer;
	}

	addLife(){
		var y = 20;
		for(var i = 0; i < this.glass_coins; i++){
	      	var x = 10 + (50 * i);
		    var percentSprite = new Life(this.game,x , y);
		    percentSprite.width = 20;
		    percentSprite.height = 20;
		    percentSprite.scale.setTo(this.scaleRatio / 10);
		    this.lifesRemaining.push(percentSprite);
		    this.game.add.existing(percentSprite);
	    }
	}

	scramble(){

		var text_array = this.shuffle(this.idea.split(" "));
		this.idea_length = text_array.length;

    	var margin_x = 10;
    	var margin_y = this.game.world._height - (this.game.world._height - 100);
    	var result = [];
    	for(var i = 0; i < text_array.length; i++ ){
    		if(i != 0) {
	          var margin_x = margin_x + result[i - 1].width+20;          
	        }

	        if(margin_x >= (this.game.width - 80)){
	          margin_y += 100;
	          margin_x = 20;
	        }

	    	var text = new Button(this.game, margin_x , this.game.world.centerY + margin_y , text_array[i], this);
	    	text.scale.setTo(this.scaleRatio, this.scaleRatio);
       		this.answer_initial_position[text_array[i]] = { "x": text.x , "y" : text.y,"clicked": 1 };
  		
	    	result.push(text);
		    this.game.add.existing(text);
    	}
	}

	shutdown(){
		this.question = "";
		this.lifesRemaining = [];
		this.score = 0;
		this.glass_coins = 0;
		this.timer  = 0;
		this.answer = [];
		this.answer_initial_position = {};
		this.idea_length = 0;
		this.widthStore = 0;
		this.heightStore = 0;
		this.widthStore = 0;
		this.heightStore = 0;
		console.log("destroyed them all :)");
	}

	create(){
    	
    	var background  = this.game.add.tileSprite(0 , 0,this.game.width , this.game.height, 'background');	    
    	this.question = this.game.add.text(this.game.world.centerX , this.game.world._height - (this.game.world._height - 160), this.fact , {fill:"#ffffff", align : "center", wordWrap: true, wordWrapWidth: window.innerWidth});
    	this.question.anchor.setTo(0.5);
    	this.question.scale.setTo(this.scaleRatio);
    	//load audio files
    	this.click_audio =  this.game.add.audio('click_audio');
    	this.correct_answer = this.game.add.audio('correctanswer');
    	this.wrong_answer_audio = this.game.add.audio('wronganswer');

    	this.scramble();
    	this.addLife();
    	this.addTimer();
    	this.addScore();

	}

	shuffle(array) {
	    var currentIndex = array.length, temporaryValue, randomIndex;
	    // While there remain elements to shuffle...
	    while (0 !== currentIndex) {
	      // Pick a remaining element...
	      randomIndex = Math.floor(Math.random() * currentIndex);
	      currentIndex -= 1;
	      // And swap it with the current element.
	      temporaryValue = array[currentIndex];
	      array[currentIndex] = array[randomIndex];
	      array[randomIndex] = temporaryValue;
	    }
	    return array;
  	}


  	buttonClicked(item){
  		var new_position = this.calculatePositionWhenClicked(item);
    //item.text = "hello";
    	console.log("new_position");
    	console.log(new_position);

	    item.x = new_position.x;
	    item.y = new_position.y;

	    this.click_audio.play();
	    
	    console.log(item.x);
	    console.log(item.text);
	    //alert("hello");
	    console.log("over");
  		if(this.answer.length == this.idea_length){
  			this.checkAnswer();
  		}
	    //text.fill = '#ff00ff'; 		
  	}

  	checkAnswer(){
	      var string = this.answer.join(" ");
	       this.time.destroy();
	      if(string == this.idea){
	      	console.log("correct");
	        //window.alert("correct");
	        this.game.state.restart();
	        var score = this.percentage + this.calculatePercentagePerFact();
	        //console.log("score");
	        //console.log(score);
	        this.correct_answer.play();
	        this.game.state.start('rightanswer',true,false,this.data,this.note_index,this.index + 1, this.glass_coins, score);
	      } else {
	      	this.wrong_answer_audio.play();
	        this.game.state.start('wronganswer',true,false,this.data,this.note_index,this.index + 1, this.glass_coins - 1, (this.percentage - this.calculatePercentagePerFact()), string);
	      	//console.log("wrong");
	        //window.alert("wrong");
	      }
	}

	calculatePercentagePerFact(){
		var length = this.notes.facts.length;
		console.log("calculatePercentagePerFact");
		console.log(length);
		if(100 % length == 0){
			return (100 / length);
		} else {
			return Math.floor(100 / length);
		}
	}

  	calculatePositionWhenClicked(item){
  		console.log("answer_initial_position");
  		console.log(this.answer_initial_position);

	    this.answer_initial_position[item.text].clicked += 1;
	    if(this.answer_initial_position[item.text].clicked % 2 == 1){
	    	if(item.text == this.answer[this.answer.length - 1]){
	    		var index = this.answer.indexOf(item.text);
			    this.answer.splice(index , 1);
			    console.log(this.answer);
			    return this.calculatePositionBackClicked(item);
	    	} else {

	    		return {'x' : item.x , 'y' : item.y};
	    	}
	      
	    }
	    //answer
	    this.answer.push(item.text);    
	    //position
	    var position;

	    var initialx = 0;
	    var initialy = (this.game.world._height - (this.game.world._height - 200));

	    var margin_y = 0;
	    var margin_x = this.widthStore + 20;

	    if(this.answer.length != 0 ){
	      if(margin_x >= (this.game.width - 50)){
	        this.heightStore += 100;
	        this.widthStore = 0;
	        margin_x = this.widthStore;
	      }
	      position = {'x': margin_x, 'y': initialy + this.heightStore}
	    } else {
	      position = {'x' : initialx , 'y' : initialy};
	    }
	    this.widthStore += item.width + 20;
	    var text = item.text;
	    console.log(this.answer_initial_position);
	    //var obj = {}
	    
	    return position;
  }


	calculatePositionBackClicked(item){
	    this.widthStore -= (item.width + 20);
	    console.log("calculatePositionBackClicked");
	    let item_text = item.text;
	    let reposition = this.answer_initial_position[item_text];
	    console.log(reposition);
	    let item_x = reposition["x"];
	    let item_y = reposition["y"];
	    return {"x" : item_x , "y" : item_y};
	    // item.x = this.answer_initial_position[item_text].x;
	    // item.y = this.answer_initial_position[item_text].y;
	}

}