export class RightAnswerState extends Phaser.State {

	constructor(){
		super();
		this.quotes = ["PRESCHOOLER","MIDDLESCHOOLER","JUNIOR","SENIOR", "FRESHMAN", "SOPHOMORE", 
		"GRADUATE", "MASTERS", "DOCTOR", "PROFFESSOR"];
		this.scaleRatio = window.devicePixelRatio / 1.2;

	}

	init(data,note_index,index,glass_coins, percentage){
		console.log("percentage");
		console.log(percentage);
		this.data = data;
		this.note_index = note_index,
		this.notes = this.data.notes[this.note_index];
		console.log("right");
		console.log(this.data);
		console.log(this.note_index);
		console.log(this.notes);
		this.level = this.notes.level;
		this.fact_index = index;
		this.glass_coin = glass_coins;
		this.percentage = percentage;
	

	}

	preload(){

	}
	
	create(){
    	var background  = this.game.add.tileSprite(0 , 0,this.game.width , this.game.height, 'background');	 
	    var correct_background = this.game.add.sprite(this.game.world.centerX - 180, this.game.world.centerY - 250 ,'correct_image');
	    correct_background.scale.setTo(this.scaleRatio);
    	var right_text = this.game.add.text(this.game.world.centerX-30, this.game.world.centerY - 90, "Next",{fill: "#ffffff"});
		right_text.scale.setTo(this.scaleRatio);
		
		var winner = this.game.add.text(this.game.world.centerX-80, this.game.world.centerY + 50 , "You have",{fill: "#ffffff", "font": "30px Arial"});
		winner.scale.setTo(this.scaleRatio);
		var percent = this.game.add.text(this.game.world.centerX - 50, this.game.world.centerY + 100, this.percentage + "%", {fill: "#ffffff","font": "30px Arial"});
    	percent.scale.setTo(this.scaleRatio);
    	this.click_audio =  this.game.add.audio('gamebutton');

		right_text.inputEnabled = true;
	    right_text.padding.set(0,10);
	    right_text.events.onInputDown.add(this.next, this);
	}

	next(item) {
		this.click_audio.play();
		console.log("RightAnswerState");
		console.log(this.fact_index);
		console.log(this.notes.facts.length - 1);
		if(this.fact_index == this.notes.facts.length) {
			if(this.percentage >= this.calculateRequiredPercentage(this.level) || this.glass_coin == 0){
				this.game.state.start('finished',true,false,this.data,this.level,this.note_index,this.fact_index,this.calculateRequiredPercentage(this.level), this.glass_coin);
			} else {
				this.game.state.start('gameover',true,false,this.data,this.note_index,this.calculateRequiredPercentage(this.level), this.glass_coin);
			}
			//go to game over page or success page depending on if percentage score for level met
			//alert("next");
		} else {
			this.game.state.start('maingame',true,false,this.data,this.note_index,this.percentage ,this.glass_coin, this.fact_index);
		}
	}

	calculateRequiredPercentage(level){
		if(level == 0){
			return 55;
		}

	   	var required = 55 + ((level - 1) * 5);
	    return required;
  	}

}