
export class PlayGameState extends Phaser.State {

	constructor(game,note_index,data){
		super();
		this.data = data;
		this.note_index = note_index;
		this.notes = this.data.notes[note_index];
		this.level = this.notes.level;
	}

	preload(){

		this.game.load.image('background','build/assets/image/deep-space.jpg');
    	this.game.load.image('play_button','build/assets/image/g1032.png');
    	this.game.load.image('background','build/assets/image/deep-space.jpg');
	    this.game.load.image('play_button','build/assets/image/g1032.png');
	    this.game.load.image('button_background','build/assets/image/version-button.png');
	    this.game.load.image('percent_image','build/assets/image/percentage.png');
	    this.game.load.image('correct_image','build/assets/image/correct.png');
	    this.game.load.image('wrong_image','build/assets/image/wrong.png');
	    //level image assets
	   
	    this.game.load.image('glass_coin','build/assets/image/glasscoin.png');
	    this.game.load.spritesheet('button', 'build/assets/image/button_sprite_sheet.png', 193, 71);

	    //audio files
	    //this.game.load.audio('play_audio','build/assets/audio/battery.wav');
	    // this.game.load.audio('click_audio','build/assets/audio/tapword.ogg');
	    // this.game.load.audio('coin_gain','build/assets/audio/coinsgain.ogg');
	    // this.game.load.audio('congratulation','build/assets/audio/congratulations.ogg');
	    // this.game.load.audio('correctanswer','build/assets/audio/correctanswer.ogg');
	    // this.game.load.audio('failure','build/assets/audio/failure.ogg');
	    // this.game.load.audio('gamebutton','build/assets/audio/gamebuttons.ogg');
	    // this.game.load.audio('gameover','build/assets/audio/gameover.ogg');
	    // this.game.load.audio('gamestart','build/assets/audio/gamestart.ogg');
	    // this.game.load.audio('glasscoinincrease','build/assets/audio/glasscoinincrease.ogg');
	    // this.game.load.audio('greatejob','build/assets/audio/greatejob.ogg');
	    // this.game.load.audio('wronganswer','build/assets/audio/wronganswer.ogg');
	    

	}

	create(){
    	
		console.log("centerX");
    	console.log(this.game.world.centerX);

    	var background  = this.game.add.tileSprite(0 , 0,this.game.width , this.game.height, 'background');
    	var game_title  = this.game.add.text(this.game.world.centerX - 180,this.game.world.centerY - 250, 'Knowledge Reinforcement',{fill : "#ffffff"});
	    game_title.stroke = "#FF4081";
	    game_title.strokeThickness = 5;
	    game_title.setShadow(2, 2, "#303F9F", 20, true, false);
    	//this.click_audio =  this.game.add.audio('click_audio');
    	//this.gamestart = this.game.add.audio('gamestart');
    	//this.gamestart.play();

    	var demo_level = this.level + 1;
	    var note_title    = this.game.add.text((this.game.world.centerX - 200), this.game.world.centerY - 200 , this.title,{fill : "#ffffff","font": "24px Arial"});
	    var note_topic    = this.game.add.text(this.game.world.centerX - 50 , this.game.world.centerY - 170,this.topic, {fill: "#ffffff","font": "20px Arial"});
	    var current_level = this.game.add.text(this.game.world.centerX - 80 , this.game.world.centerY - 100,"LEVEL "+demo_level,{fill : "#ffffff"});
	    var pre_game_title = this.game.add.text(this.game.world.centerX - 120, this.game.world.centerY - 60, "You need a minimum score of",{fill: "#ffffff", "font": "18px Arial"});
	    var required_score = this.game.add.text(this.game.world.centerX - 50, this.game.world.centerY - 30, this.calculateRequiredPercentage(this.level)+"%", {fill: "#ffffff"});
	    var complete_level = this.game.add.text(this.game.world.centerX - 100, this.game.world.centerY, "to complete this level",{fill: "#ffffff", "font": "20px Arial","wordWrap":true,wordWrapWidth: this.game.width,"align":"center"});
	    var play_button    = this.game.add.button(this.game.world.centerX - 110 ,this.game.world.centerY + 50,'play_button',this.play,this , 2 , 1, 0);
	    play_button.width = 220;
	    play_button.height = 80;  
	}	

	calculateRequiredPercentage(level){
	   	var required = 55 + (level * 5);
	    return required;
  	}

  	play(){
    	//this.click_audio.play();
  		this.game.state.start('maingame',true,false,this.data,this.note_index,0, 5 , 0);
  	}
}