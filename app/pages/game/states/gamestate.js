class GameState extends Phaser.State {

	create() {
		let center = { x: this.game.world.centerX, y: this.game.world.centerY }
		let text = this.game.add.text(this.game.world.centerX, this.game.world.centerY, "- phaser -\nwith a sprinkle of\nES6 dust!",
			{fill:"#ffffff"});
		text.anchor.set(0.5);
	}

}
export default GameState;
