export class FinishedState extends Phaser.State {
	constructor(nav , tabspage,userservice){
		super();
		this.nav = nav;
		this.tabspage = tabspage;
		this.userservice = userservice;
		this.images = ["preschooler","middleschooler",
		    "junior","senior","freshman","sophomore","graduate",
		    "masters","doctor","prof"];
		this.quotes = ["PRESCHOOLER","MIDDLESCHOOLER","JUNIOR","SENIOR", "FRESHMAN", "SOPHOMORE", 
		"GRADUATE", "MASTER", "DOCTOR", "PROFFESSOR"];
		this.scaleRatio = window.devicePixelRatio / 1.2;

	}


	init(data,level,note_index,index, percentage, glass_coin){
		this.data = data;
		this.note_index = note_index;
		this.fact_index = index;
		this.percentage = percentage;
		//this.requiredPercentage = requiredPercentage;
		this.notes = this.data.notes[this.note_index];
		this.level = this.notes.level;
		this.glass_coin = glass_coin;
		this.saveData();
	}

	preload(){
	    this.game.load.audio('gamebutton','build/assets/audio/gamebuttons.ogg');
	    this.game.load.audio('congratulation','build/assets/audio/congratulations.ogg');

		this.game.load.image('nextlevel_button','build/assets/image/nextlevel.png');
    	this.game.load.image('later_button','build/assets/image/later.png');
    	this.game.load.image('done_button','build/assets/image/done.png');

	}

	create(){
    	var background  = this.game.add.tileSprite(0 , 0,this.game.width , this.game.height, 'background');	 
		var text = this.game.add.text(this.game.world.centerX - 150, this.game.world.centerY - 300, "Congratulations",{fill : "#ffffff",wordwrap:true, wordwrapWidth: this.game.width, align: "center"});
		text.scale.setTo(this.scaleRatio);
		var image = this.game.add.sprite(this.game.world.centerX- 130 , this.game.world.centerY - 170 , this.images[this.level]);
		// image.scale.setTo(this.scaleRatio);
		
		var winner_text =  this.game.add.text(this.game.world.centerX - 200, this.game.world.centerY + 50, "You are now a "+this.quotes[this.level],{font: "20px Arial",fill : "#ffffff",wordwrap:true, wordwrapWidth: this.game.width, align: "center"});
		winner_text.scale.setTo(this.scaleRatio);
		var winner = this.game.add.text(this.game.world.centerX - 200,this.game.world.centerY+100," Bonus Points: "+this.calculateCoinAndPointLevel(this.level)+" Bonus Coins: "+this.calculateCoinAndPointLevel(this.level),{font: "20px Arial",fill : "#ffffff",wordwrap:true, wordwrapWidth: this.game.width, align: "center"});
		winner.scale.setTo(this.scaleRatio);
		var congrats = this.game.add.audio('congratulation');
		congrats.play();
		
		if(this.data.notes[this.note_index].level != 10){
			var tryagain_button  = this.game.add.button(this.game.world.centerX - 250 ,this.game.world.centerY + 160,'later_button', this.goTohomePage ,this , 2 , 1, 0);
			tryagain_button.scale.setTo(this.scaleRatio);
			var later_button   = this.game.add.button(this.game.world.centerX + 10,this.game.world.centerY + 160,'nextlevel_button',this.nextlevel,this , 2 , 1, 0);  
			later_button.scale.setTo(this.scaleRatio);
		} else {
			var done_button    = this.game.add.button(this.game.world.centerX - 120 ,this.game.world.centerY + 160,'done_button',this.goTohomePage,this , 2 , 1, 0);
		    done_button.width = 220;
		    done_button.height = 80;
		    done_button.scale.setTo(this.scaleRatio);
		}

    	this.click_audio =  this.game.add.audio('gamebutton');
	}

	goTohomePage(item){
		this.nav.push(this.tabspage);
		this.click_audio.play();
	}

	saveData(){
		if(this.data.notes[this.note_index].level == 10){
			this.data.notes[this.note_index].played += 1;
			this.data.glass_coins += ( this.calculateCoinAndPointLevel() - ( 5 - parseInt(this.glass_coin) ) ) ;
			this.data.glass_points += this.calculateCoinAndPointLevel();
			this.userservice.save(this.data);
			this.userservice.points(this.calculateCoinAndPointLevel());
		} else {
			this.data.notes[this.note_index].level += 1;
			this.data.notes[this.note_index].played += 1;
			this.data.glass_coins += ( this.calculateCoinAndPointLevel() - ( 5 - parseInt(this.glass_coin) ) ) ;
			this.data.glass_points += this.calculateCoinAndPointLevel();
			this.userservice.save(this.data);
			this.userservice.points(this.calculateCoinAndPointLevel());
		}
	}


	nextlevel(){
		this.userservice.getCoin((coin)=>{
			if(coin >= 5){
				this.game.state.start('PlayState',true,false);
				this.click_audio.play();
			} else {
				window.alert("Sorry you don't have enough coins");
			}
		});
	}

	calculateCoinAndPointLevel(){
		return (2 * parseInt(this.level + 1));
	}

	
}