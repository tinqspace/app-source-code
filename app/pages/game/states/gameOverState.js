export class GameOverState extends Phaser.State {
	constructor(nav , tabspage, userservice){
		super();
		this.nav = nav;
		this.tabspage = tabspage;
		this.userservice = userservice;
		this.scaleRatio = window.devicePixelRatio / 1.2;

	}

	init(data,note_index,requiredPercentage, glass_coin){
		this.data = data;
		this.note_index = note_index;
		this.requiredPercentage = requiredPercentage;
		this.glass_coin = glass_coin;
	}

	preload(){
		this.game.load.image('tryagain_button','build/assets/image/tryagain.png');
    	this.game.load.image('nextlevel_button','build/assets/image/later.png');
	}

	create(){
		this.failure_audio =  this.game.add.audio('failure');
		this.failure_audio.play();

    	var background  = this.game.add.tileSprite(0 , 0,this.game.width , this.game.height, 'background');	 
		var text = this.game.add.text(this.game.world.centerX - 50, this.game.world.centerY - 150, "Oops",{fill : "#ffffff",wordwrap:true, wordwrapWidth: this.game.width, align: "center", font: "48px Arial"});
		text.scale.setTo(this.scaleRatio, this.scaleRatio);
		var text = this.game.add.text(this.game.world.centerX - 250, this.game.world.centerY - 30, "Looks like you did not meet your \n learning goal for this level",{fill : "#ffffff",wordwrap:true, wordwrapWidth: this.game.width, align: "center",fontWeight: "bold",font: "20px Arial"});
		text.scale.setTo(this.scaleRatio, this.scaleRatio);
		var tryagain_button = this.game.add.button(this.game.world.centerX - 250 ,this.game.world.centerY + 100,'tryagain_button', this.tryagain ,this , 2 , 1, 0);
		tryagain_button.scale.setTo(this.scaleRatio, this.scaleRatio);
		var later_button = this.game.add.button(this.game.world.centerX + 10,this.game.world.centerY + 100,'nextlevel_button',this.goTohomePage ,this , 2 , 1, 0);  
    	later_button.scale.setTo(this.scaleRatio, this.scaleRatio);
    	this.click_audio =  this.game.add.audio('gamebutton');
	}

	goTohomePage(){
		console.log("game");
		console.log((5 + parseInt(this.glass_coin)));
		console.log(this.glass_coin);
		this.data.glass_coins -= (5 + (5 - parseInt(this.glass_coin)));
		this.userservice.save(this.data);
		// this.nav.push(this.tabspage);
		// this.nav.setRoot(this.tabspage);
		this.nav.push(this.tabspage);
		this.click_audio.play();
	}

	tryagain(){
		this.userservice.getCoin((coin)=>{
			if(coin >= 5){
				this.game.state.start('PlayState',true,false);
				this.click_audio.play();
			} else {
				window.alert("Sorry you don't have enough coins");
				return;
			}
		});
	}
}