import {Page, NavController,NavParams} from 'ionic-angular';
import {Http} from 'angular2/http';
import {TabsPage} from '../tabs/tabs';
import {UserService} from '../../providers/user/user';
import {DatabaseService} from '../../providers/database/database';
import {Connectivity} from '../../providers/connectivity/connectivity';

import {ServerService} from '../../providers/server/server';
import {MainGame} from './main';
import {PlayGameState} from './states/playgamestate';
import {MainGameState} from './states/mainGameState';
import {WrongAnswerState} from './states/wrongAnswerState';
import {RightAnswerState} from './states/rightAnswerState';
import {FinishedState} from './states/finishedState';
import {GameOverState} from './states/gameOverState';
import {GameState} from './states/gamestate';


@Page({
  templateUrl: 'build/pages/game/game.html',
  providers: [UserService,Connectivity,ServerService],
})

export class GamePage {
  
  static get parameters() {
    return [[NavController],[Http],[NavParams],[UserService],[Connectivity],[ServerService]];
  }

  random(){
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var text = "";
      for( var i=0; i < 4; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
  }

  onPageWillEnter(){
  
  }

  constructor(nav,http,params,userservice,connectivity,server) {
    this.id = params.get("id");
    this.note_index = params.get('note_index');
    var scaleRatio = window.devicePixelRatio / 3;
    //this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    // //var note_index = 0;
    //this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    // this.scale.minWidth = 320;
    // this.scale.minHeight = 480;
    // this.scale.maxWidth = 768;
    // this.scale.maxHeight = 1152;
    this.data = userservice.getData();
    this.server = server;
    this.userservice = userservice;
    this.connectivity = connectivity;

    this.nav = nav;
    window.localStorage.setItem('note_index',this.note_index);
    
    var elem = params.get("id");
    var getbyid = document.getElementById(elem);
    if(getbyid == null){
      elem = "mygame";
    }
  }

  onPageDidEnter(){
    this.game = new Phaser.Game(window.innerWidth * (window.devicePixelRatio / 1.1), (window.innerHeight * window.devicePixelRatio / 1.1), Phaser.CANVAS,this.id, null);
    //this.game.scale.updateLayout()
    this.game.state.add('PlayState',new PlayGameState(), false);
    this.game.state.add('maingame', new MainGameState(), false);
    this.game.state.add('wronganswer',new WrongAnswerState(),false);
    this.game.state.add('rightanswer', new RightAnswerState(),false);
    this.game.state.add('gameover', new GameOverState(this.nav, TabsPage, this.userservice),false);
    this.game.state.add('finished', new FinishedState(this.nav, TabsPage, this.userservice),false);
    this.game.state.add('GameState', GameState, false);
    this.game.state.start('PlayState',true,false);

  }

  onPageWillLeave(){
    this.game.cache.destroy(); 
    this.userservice.init();
    if(this.connectivity.isOnline()){
        new Promise(resolve => {
                console.log("Promise resolving for isOnline");
                console.log()
                var mydata = this.userservice.getData();
                var notes = mydata.notes;
                var email = mydata.email;
                var facebook_id = mydata.facebook_id;
                var point = this.userservice.getPoint();
                this.server.sendNote(notes, mydata,()=>{
                   this.server.transferPoints(email,point,()=>{
                     this.userservice.resetPoint();
                   });
                   this.server.getGiftCard(facebook_id,(data)=>{
                        console.log("get gift gift_card");
                        console.log(data);
                        console.log(data.data[0].gift_card);
                       this.dbService.addGiftCard(data.data[0].gift_card);
                   });
                   console.log("Done with promise resolving");
                 });
        });
    } 
  }
}