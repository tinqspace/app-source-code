import {MainGameState} from '../states/mainGameState';
export class Button extends Phaser.Text {
	
	constructor(game,marginX,marginY, text,that){
		super(game,marginX, marginY,text,{ font: "24px Arial",fontWeight : 'bold', fill: "#303F9F", align: "center", backgroundColor: "#ffffff" });
		this.game = game;
		this.inputEnabled = true;
       	this.padding.set(0,10);
       	this.events.onInputDown.add(new MainGameState().buttonClicked,that);
	}

	// buttonClicked(item){
	// 	alert("button clicked from button class");
 //  		this.game.state.start('wronganswer',true,false,"jjjhjh");  		
	// }
}