export class Life extends Phaser.Sprite {
	constructor(game,x,y){
		super(game,x ,y, 'glass_coin');
	}
	
	hide(){
		this.visible = false;
	}
}