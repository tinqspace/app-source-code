import {PlayGameState} from './states/playgamestate';
import {MainGameState} from './states/mainGameState';
import {WrongAnswerState} from './states/wrongAnswerState';
import {RightAnswerState} from './states/rightAnswerState';
import {FinishedState} from './states/finishedState';
import {GameOverState} from './states/gameOverState';

export class MainGame extends Phaser.Game {
	constructor(note_index,nav,data,TabsPage){
		console.log("working on ame");
		super(window.innerWidth * window.devicePixelRatio, 
      (window.innerHeight * window.devicePixelRatio)* window.devicePixelRatio, Phaser.AUTO, 'game', null);
		this.state.add('playgame', new PlayGameState(note_index,data),false);
	     this.state.add('maingame',new MainGameState(),false);
	      this.state.add('wronganswer',new WrongAnswerState(),false);
	      this.state.add('rightanswer', new RightAnswerState(),false);
	      this.state.add('gameover', new GameOverState(nav, TabsPage, this.userservice),false);
	      this.state.add('finished', new FinishedState(nav, TabsPage, this.userservice),false);
	      this.state.start('playgame');
	}
}