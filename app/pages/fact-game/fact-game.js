import {Page, NavController,NavParams,Modal,Alert} from 'ionic-angular';
import {HelpIdeaModal} from '../modal/help-idea-modal/help-idea-modal';
import {DatabaseService} from '../../providers/database/database';
import {UserService} from '../../providers/user/user';
import {SuccessAnswerNoteModal} from '../modal/success-answer-note-modal/success-answer-note-modal';
import {TabsPage} from '../tabs/tabs';

@Page({
  templateUrl: 'build/pages/fact-game/fact-game.html',
  providers: [DatabaseService,UserService]

})
export class FactGamePage {

  static get parameters() {
    return [[NavController],[NavParams],[DatabaseService],[UserService]];
  }
  
  createImageModel(){
    for(let i = 0; i < this.count; i++){
      this.images.push("ideacoin.png");
    }  
  }
  
  change(i){
    if(i == 9){
      //increment glass coin
      this.glassCoin++;
      this.images = [];
      this.count = 0;
      // console.log(this.images);
      this.showImage();
      this.createImageModel();
    } else if(i > 9){
      var index = i - this.deduction;
      // console.log('chnage index'+index);
      this.images[index] = "knowledgecoin.png";
    } else {

      this.images[i] = "knowledgecoin.png";
    }
  }

  showImage(){
    //var remaininig = this.facts_length % 10;
    if(this.images_length < 10){
      this.count = this.images_length;
    } else {
      this.deduction += 10;
      this.images_length -= 10;
      this.count += 10;
    }
  }

  constructor(nav,params,dbService,userservice) {

    this.note = params.get('note');
    this.edit = params.get('edit') || false;
    this.position = params.get('position');

    this.facts = this.note['facts'];

    this.factGameNote = [];
    this.images = [];
    this.factGameNote.push(this.facts[0]);
    this.facts_length = this.facts.length;
    this.images_length = this.facts.length;
    
    // this.sound = new MediaPlugin("build/assets/audio/battery.wav",
    //   function(){console.log("successs");},
    //   );


    this.loaded_facts = 0;
    this.deduction = 0;
    //this.possible_glass_coins = Math.floor(this.facts_length / 10);
    this.possible_glass_coins = Math.floor();


    console.log("possible_glass_coins");
    //console.log(this.possible_glass_coins);

    this.userservice = userservice;
    
    this.dbService = dbService;
    //console.log("entered identiy");
    //console.log(this.facts);
    this.nav = nav;
    this.glassCoin = 0;
    this.index = 0;

    this.finished = false;

    this.count = 0;
    this.showImage();
    this.createImageModel();
    this.change(0);
  }

  maximum_error() {
      let  alert = Alert.create({ 
      title: 'Sorry', 
      subTitle: 'You are not allowed to input more than 100 characters', 
      buttons: ['Ok'] 
    }); 
    this.nav.present(alert);
  }

  nextFact(){    
    if(this.factGameNote[this.index].answer.length > 100){
      this.maximum_error();
      return;
    }

    this.index++
    this.change(this.index);

    if(this.index == (this.facts_length - 1)){
      //change image
      this.finished = true;
    }
      this.factGameNote.push(this.facts[this.index]);
      //this.sound.play();
  }
  
  helpModal(){
  	let success = Modal.create(HelpIdeaModal);
    this.nav.present(success);
  	console.log("entered");
  }

  home(){
    console.log("fact in home area");
    this.dbService.get('demo').then((response) => {
      console.log("getting home response");
      this.note.facts = this.facts;
      console.log("nots in home"+JSON.stringify(this.note));
        var data = response;
        data.notes.push(this.note);
        console.log("create");
        this.dbService.save(data);
        this.userservice.init();
        //this.viewCtrl.dismiss();
      this.nav.setRoot(TabsPage);
      this.nav.push(TabsPage);
      }).catch((error)=> {
        let alert = Alert.create({
          title: 'Sorry!',
          subTitle: 'Could not save note. Please try again.',
          buttons: ['OK']
        });
      });
  }

  add(){

  }

  save(){
    this.note['facts'] = this.facts;
    var data;

    this.dbService.get('demo').then((response) => {
      data = response;
      this.note.status = 1;
      data.notes.splice(this.position, 0, this.note);
      // console.log("data0000");
      // console.log("Glass pages fact game");
      this.note['facts'] = this.facts;
      let success_note = Modal.create(SuccessAnswerNoteModal,{note : this.note, edit: this.edit, position: this.position});
      this.nav.present(success_note);
      this.dbService.save(data);
      this.userservice.init();
      
    }).catch((error)=> {
      let alert = Alert.create({
        title: 'Sorry!',
        subTitle: 'Could not save note. Please try again.',
        buttons: ['OK']
      });
      this.nav.present(alert);
    });
    //console.log("entered");
  }
  
}
