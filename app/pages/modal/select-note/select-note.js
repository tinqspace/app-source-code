import {Page,ViewController,NavController,NavParams, Alert, Modal} from 'ionic-angular';
import {IdeaGamePage} from '../../idea-game/idea-game';
import {ViewNotePage} from '../../view-note/view-note';
import {TabsPage} from '../../tabs/tabs';
import {GamePage} from '../../game/game';
import {ContinueNoteModal} from '../continue-note/continue-note';
import {EditNoteModal} from '../edit-note/edit-note';
import {SuccessExtractFacts} from '../success-extract-facts/success-extract-facts';
import {SuccessCreateNote} from '../success-create-note/success-create-note';
import {DatabaseService} from '../../../providers/database/database';
import {UserService} from '../../../providers/user/user';

@Page({
	templateUrl:'build/pages/modal/select-note/select-note.html',
})

export class SelectNoteModal {
	static get parameters(){
		return [[NavController],[ViewController],[NavParams],[UserService],[DatabaseService]];
	}

	onPageWillEnter(){
		this.userservice.init();
	}

	random(){
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		var text = "";
	    for( var i=0; i < 4; i++ )
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	    return text;
	}
	
	constructor(nav,view,params,userservice,dbService){
		console.log("Note modal");
		this.navCtrl = nav;
		this.viewCtrl = view;
		this.userservice = userservice;
		this.note_index = params.get('note_index');
		// console.log("db service");
		console.log("Note modal");

		this.data = userservice.getData();
		// console.log("db service after");
		
		// console.log("data");
		// console.log(this.data);
		this.note = this.data.notes[this.note_index];

		this.images = ["pending.png","1preschooler.png","2middleschooler.png",
		"3junior.png","4senior.png","5freshman.png","6sophomore.png","7graduate.png",
		"8masters.png","9doctor.png","10prof.png"];
		this.quotes = ["","PRESCHOOLER","MIDDLESCHOOLER","JUNIOR","SENIOR", "FRESHMAN", "SOPHOMORE", 
		"GRADUATE", "MASTER", "DOCTOR", "PROFFESSOR"];
		this.level = parseInt(this.data.notes[this.note_index].level);
		// console.log("this.level");
		// console.log(this.level);
		this.image = this.images[this.level];

		// console.log("db service");
		this.dbService = dbService;
		// console.log("db service after");

	}

	close(){
		this.viewCtrl.dismiss();
		this.navCtrl.setRoot(TabsPage).then(()=>{
			this.navCtrl.pop()
		});	
	}

	deleteNote(){
	    var current_note = this.note;
	    var index = this.data.notes.findIndex(x => x.topic == current_note.topic);
	    // console.log(index)
	    if(index < 0) return;
	    this.data.notes.splice(index,1);
	    this.dbService.save(this.data);
	    this.userservice.init();
	}

	editNote(){
		// console.log("Edit note");
		this.deleteNote();
		let modal = Modal.create(SuccessCreateNote, {note: this.note} );
		this.navCtrl.present(modal);
		this.navCtrl.dismiss();

		//l/et modal = 
	}

	playgame(){
		// console.log("playgame");
		// console.log(this.note);
		
		if(this.data.glass_coins < -200){
			let alert = Alert.create({
		      title: 'Sorry!',
		      subTitle: 'You have to have more than 5 GLASS coins to play',
		      buttons: ['OK']
		    });
		    this.navCtrl.present(alert);
			return;
		} 
		this.viewCtrl.dismiss();
		this.navCtrl.push(GamePage, {"note_index": this.note_index,"data":this.data, "id": this.random()});

		//this.viewCtrl.dismiss();
		//this.navCtrl.pop();
	}

	reactivate(){
		let confirm = Alert.create({
	      title: 'Reactivate Note',
	      message: 'You need 20 glass coins to reactivate this  note. Proceed ?',
	      buttons: [
	        {
	          text: 'No',
	          handler: () => {
	            console.log('Disagree clicked');
	          }
	        },
	        {
	          text: 'Yes',
	          handler: () => {
	          	if(this.data.glass_coins < 20){
	          		let alert = Alert.create({
				      title: 'Sorry!',
				      subTitle: 'You dont have enough glass coins',
				      buttons: ['OK']
				    });
				    this.navCtrl.present(alert);
					return;
	          	}

	            this.userservice.renew(0,this.note_index,20,()=>{
					this.userservice.init();
					this.viewCtrl.dismiss();
					this.navCtrl.push(TabsPage);
				//this.navCtrl.pop();
				});
	          }
	        }
	      ]
	    });

	    this.navCtrl.present(confirm);
	}

	extractFacts(){
		//this.navCtrl.pop();
		this.navCtrl.push(IdeaGamePage);
	}

	viewNote(){
		this.viewCtrl.dismiss();
		this.navCtrl.push(ViewNotePage, {note: this.note, view: true, "note_index": this.note_index}).then(()=>{
			this.navCtrl.pop();
		});
		//this.navCtrl.pop();
	}

}