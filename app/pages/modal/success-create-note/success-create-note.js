import {Page,ViewController,NavController,Alert,Loading,NavParams} from 'ionic-angular';
import {IdeaGamePage} from '../../idea-game/idea-game';
import {UserService} from '../../../providers/user/user';

@Page({
	templateUrl:'build/pages/modal/success-create-note/success-create-note.html',
})
export class SuccessCreateNote {
	static get parameters(){
		return [[NavController],[ViewController],[NavParams],[UserService]];
	}
	
	constructor(nav,view,params, userservice){
		this.note = params.get('note');
		this.edit = params.get('edit') || false;
		this.position = params.get('position');

		this.navCtrl = nav;
		this.userservice = userservice;
		this.mydata = userservice.getData();

		// console.log(this.navCtrl);

		this.viewCtrl = view;
	}

	close(){
		this.viewCtrl.dismiss();
	}

	extractFacts(){
		// console.log("this.edit");
		// console.log(this.edit);
		if(this.edit === false){
			this.navCtrl.push(IdeaGamePage,{note : this.note});
		} else {
			this.navCtrl.push(IdeaGamePage,{note : this.note, edit: this.edit, position : this.position});

		}
	}

	takePicture(){
		var options = {
            destinationType: window.IonicNative.Camera.DestinationType.FILE_URI,
            sourceType: window.IonicNative.Camera.PictureSourceType.CAMERA,
            encodingType: window.IonicNative.Camera.EncodingType.JPEG,
            quality:70,
            allowEdit: false,
            saveToPhotoAlbum: false
        };

		window.IonicNative.Camera.getPicture(options).then((imageURI) => {
			this.onPhotoURISuccess(imageURI);
		}
			,(error) => {
				console.log(this.navCtrl);
				var alert = Alert.create({
	      		title: error,
	      		subTitle: 'err',
	      		buttons: ['OK'] });

    	this.navCtrl.present(alert);
			});
	}

	onFail(message) {
		console.log(this.navCtrl);
		var alert = Alert.create({
	      title: message,
	      subTitle: 'err',
	      buttons: ['OK']
	    });

    	this.navCtrl.present(alert);
	}


	resOnError(error) {
		alert("failed with error code: " + error.code);
	}
	//Callback function when the file has been moved successfully - inserting the complete path
	successMove(entry) {
		callNativePlugin(entry.fullPath);
	}


	onPhotoURISuccess(imageURI){
	// 	console.log(imageURI);
		
	// 	let alert = Alert.create({
	//       title: "jello",
	//       subTitle: 'err',
	//       buttons: ['OK']
	//     });

 //    	this.navCtrl.present(alert);
	// 	var gotFileEntry = function(fileEntry) {
	// 		var gotFileSystem = function(fileSystem) {
	// 			fileSystem.root.getDirectory("OCRFolder", {
	// 				create : true
	// 			}, function(dataDir) {
	// 				var newFileName = "ocr.jpg";
	// 				// copy the file
	// 				fileEntry.moveTo(dataDir, newFileName, successMove,
	// 						resOnError);
	// 			}, resOnError);
	// 		};
	// 		// get file system to copy or move image file to
	// 		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
	// 				gotFileSystem, resOnError);
	// 	};
	// 	// resolve file system for image
	// 	window.resolveLocalFileSystemURI(imageURI, gotFileEntry, resOnError)
	// }

	// callNativePlugin(imageURI) {
	// 	// var tesseractPlugin = cordova
	// 	// 		.require('com.tesseract.phonegap.TesseractPlugin');
	// 	// tesseractPlugin.createEvent(imageURI, (callback)=>{nativePluginResultHandler(callback)});
	}

	nativePluginResultHandler(callback) {
		let alert = Alert.create({
	      title: callback,
	      subTitle: 'good',
	      buttons: ['OK']
	    });

    	this.navCtrl.present(alert);
	}

	nativePluginErrorHandler(error) {
		console.log(error);

		let alert = Alert.create({
	      title: error,
	      subTitle: 'native plugin hanlder',
	      buttons: ['OK']
	    });
    	
    	this.navCtrl.present(alert);
		alert("error: " + error);
	}

}