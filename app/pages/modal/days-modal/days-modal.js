import {Page,ViewController,NavController} from 'ionic-angular';
@Page({
	templateUrl:'build/pages/modal/days-modal/days-modal',
})
export class DaysModal {
	static get parameters(){
		return [NavController,ViewController];
	}
	
	constructor(nav,view){
		this.navCtrl = nav;
		this.viewCtrl = view;
	}

	close(){
		this.viewCtrl.dismiss();
	}
	
}