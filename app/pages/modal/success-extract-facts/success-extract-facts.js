import {Page,ViewController,NavController,NavParams,Alert,Loading} from 'ionic-angular';
import {FactGamePage} from '../../fact-game/fact-game';
import {TabsPage} from '../../tabs/tabs';
import {DatabaseService} from '../../../providers/database/database';
import {UserService} from '../../../providers/user/user';


@Page({
	templateUrl :'build/pages/modal/success-extract-facts/success-extract-facts.html',
	providers: [DatabaseService]
})

export class SuccessExtractFacts {
	static get parameters(){
		return [[ViewController],[NavController],[NavParams],[DatabaseService],[UserService]];
	}
	
	constructor(view,nav,params,dbService,UserService){
		this.note = params.get('note');
		this.edit = params.get('edit') || false;
		this.position = params.get('position');

		this.ideas_length = this.note.facts.length;
		this.viewCtrl = view;
		this.navCtrl  = nav;
		this.dbService = dbService;
		this.UserService = UserService;
		this.mydata = UserService.getData();
	}

	identifyFacts(){
		console.log(this.note);
		this.navCtrl.push(FactGamePage,{note : this.note, edit : this.edit, position : this.position});
	}

	home(){
		this.dbService.get('demo').then((response) => {
	      var data = response;
	      data.notes.push(this.note);
	      // console.log("create");
	      this.dbService.save(data);
	      this.UserService.init();
	      this.viewCtrl.dismiss();
		  this.navCtrl.setRoot(TabsPage);
		  this.navCtrl.push(TabsPage);
	    }).catch((error)=> {
	      let alert = Alert.create({
	        title: 'Sorry!',
	        subTitle: 'Could not save note. Please try again.',
	        buttons: ['OK']
	      });
	    });
	}
}