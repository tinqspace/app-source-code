import {Page,ViewController,NavController} from 'ionic-angular';
@Page({
	templateUrl:'build/pages/modal/help-extract-modal/help-extract-modal.html',
})
export class HelpExtractModal {
	static get parameters(){
		return [NavController,ViewController];
	}
	
	constructor(nav,view){
		this.navCtrl = nav;
		this.viewCtrl = view;
	}

	close(){
		this.viewCtrl.dismiss();
	}
	
}