import {Page,ViewController,NavController} from 'ionic-angular';
import {IdeaGamePage} from '../../idea-game/idea-game';
@Page({
	templateUrl:'build/pages/modal/help-idea-modal/help-idea-modal.html',
})
export class HelpIdeaModal {
	static get parameters(){
		return [NavController,ViewController];
	}
	
	constructor(nav,view){
		this.navCtrl = nav;
		this.viewCtrl = view;
	}

	close(){
		this.viewCtrl.dismiss();
	}

	extractFacts(){
		this.navCtrl.push(IdeaGamePage);
	}

}