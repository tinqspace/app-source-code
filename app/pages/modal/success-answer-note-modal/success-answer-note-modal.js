import {Page,ViewController,NavController,NavParams,Alert,Loading} from 'ionic-angular';
import {ViewNotePage} from '../../view-note/view-note';
import {UserService} from '../../../providers/user/user';
import {DatabaseService} from '../../../providers/database/database';


@Page({
	templateUrl :'build/pages/modal/success-answer-note-modal/success-answer-note-modal.html',
	providers: [UserService,DatabaseService]
})
export class SuccessAnswerNoteModal {
	static get parameters(){
		return [[ViewController],[NavController],[NavParams],[UserService],[DatabaseService]];
	}

	constructor(view,nav,params,user,dbservice){
		this.note = params.get('note');
		this.edit = params.get('edit') || false;
		this.ideas_length = this.note.facts.length;
		this.topic = this.note.topic;
		this.glass_coin = this.calculateGlassCoin();
		this.viewCtrl = view;
		this.navCtrl  = nav;
		this.userservice = user;
		this.dbservice = dbservice;
		this.mydata = {name:"samuel"};
	}

	onPageWillEnter(){
		if(this.edit === false) {
			var glass_coin = this.calculateGlassCoin();
			this.dbservice.addCoin(glass_coin);
		}
	}
	
	calculateGlassCoin(){
		if(this.ideas_length % 10 != 0 ){
			var coins = parseInt(this.ideas_length / 10) + 1;
		} else {
			var coins = this.ideas_length / 10;
		}
		return coins;
	}

	viewNote(){
		console.log(this.note);
		this.navCtrl.setRoot(ViewNotePage);
		this.navCtrl.push(ViewNotePage,{note : this.note});
	}

	home(){
		this.viewCtrl.dismiss();
		this.navCtrl.pop();
	}
}