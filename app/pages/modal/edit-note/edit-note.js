import {Page,ViewController,NavController,NavParams, Modal} from 'ionic-angular';
import {SuccessExtractFacts} from '../success-extract-facts/success-extract-facts';
import {SuccessCreateNote} from '../success-create-note/success-create-note';
@Page({
	templateUrl:'build/pages/modal/continue-note/continue-note.html',
})
export class EditNoteModal {
	static get parameters(){
		return [[NavController],[ViewController],[NavParams]];
	}
	
	constructor(nav,view,navparams){
		this.note = navparams.get('note');
		this.navCtrl = nav;
		this.viewCtrl = view;
	}

	close(){
		this.viewCtrl.dismiss();
	}

	deleteNote(){
	    var current_note = this.page2_notes[i];
	    var index = this.data.notes.findIndex(x => x.topic == current_note.topic);
	    // console.log(index)
	    this.data.notes.splice(index,1);
	    this.dbService.save(this.data);
	    this.userService.init();
	}

	edit(){
		// console.log("continue");
		// console.log(this.note);
		let modal = Modal.create(SuccessExtractFacts,{note: this.note});
		this.navCtrl.present(modal);
		// if(this.note.state == "facts"){
		// 	let modal = Modal.create(SuccessExtractFacts,{note: this.note});
		// 	this.navCtrl.present(modal);
		// } else if (this.note.state == "ideas"){
		// 	let modal = Modal.create(SuccessCreateNote, {note: this.note} );
		// 	this.navCtrl.present(modal);
			
		// }
	}
	
}