import {Page,ViewController,NavController,NavParams, Modal} from 'ionic-angular';
import {SuccessExtractFacts} from '../success-extract-facts/success-extract-facts';
import {SuccessCreateNote} from '../success-create-note/success-create-note';
@Page({
	templateUrl:'build/pages/modal/continue-note/continue-note.html',
})
export class ContinueNoteModal {
	static get parameters(){
		return [[NavController],[ViewController],[NavParams]];
	}
	
	constructor(nav,view,navparams){
		this.note = navparams.get('note');
		this.navCtrl = nav;
		this.viewCtrl = view;
	}

	close(){
		this.viewCtrl.dismiss();
	}

	continue(){
		// console.log("continue");
		// console.log(this.note);
		
		if(this.note.state == "facts"){
			let modal = Modal.create(SuccessExtractFacts,{note: this.note});
			this.navCtrl.present(modal);
			return;
		} else if (this.note.state == "ideas"){
			let modal = Modal.create(SuccessCreateNote, {note: this.note} );
			this.navCtrl.present(modal);
			return;	
		}
		
		let modal = Modal.create(SuccessCreateNote, {note: this.note} );
		this.navCtrl.present(modal);

	}
	
}