import {Page, NavController,NavParams, Alert,Loading} from 'ionic-angular';
import {ServerService} from '../../providers/server/server';
import {DatabaseService} from '../../providers/database/database';
import {Connectivity} from '../../providers/connectivity/connectivity';

@Page({
  templateUrl: 'build/pages/gift-card/gift-card.html',
  providers: [ServerService,DatabaseService,Connectivity]
})
export class GiftCardPage {
  static get parameters() {
    return [[NavController],[NavParams],[ServerService],[DatabaseService],[Connectivity]];
  }
  constructor(nav,params,server,dbservice, connectivity) {
    this.nav = nav;
    this.data = params.get('data');
    this.flag = params.get('flag');
    this.server = server;
    this.dbservice = dbservice;
    this.gift_card = this.data.gift_card;
    this.connectivity = connectivity;
    this.amount = "";
    this.phone = "";
    this.merchant = "";
  }

  make(){
    let loading = Loading.create({
          spinner: 'crescent',
          content: 'Please wait',
        });  
      this.nav.present(loading);

      if(this.amount=="" || this.phone=="" || this.merchant==""){
          let alert = Alert.create({
            title: 'Sorry',
            subTitle: 'Please fill in every details',
            buttons: ['OK']
          });
          this.nav.present(alert);
          return;
      }

      var form_data = "facebook_id="+this.data.facebook_id+"&amount="+this.amount+"&phone="+this.phone+"&merchant="+this.merchant;
    
      this.server.post('gift/withdraw',form_data, (error,response)=> {
          console.log("withdraw");
          console.log("data"+JSON.stringify(response));
          if(error){
            this.message = error.message;
            loading.dismiss(function(){});
            return;
          } else {
            this.deduct(this.amount);
            this.message = "You have successfully withdrawn "+response.data.amount+" gift card. You will be contacted within 48 hours.";  
            loading.dismiss();
          }
      });
  }

  deduct(amount){
    this.dbservice.deductGiftCard(amount);
  }


  withdraw(){
    if(this.connectivity.isOffline()){
      let alert = Alert.create({
        title: 'Hi',
        subTitle: 'Please enable internet connection on your phone',
        buttons: ['OK']
      });
      
      this.nav.present(alert);
      return;
    }

  	if(this.gift_card < 1000){
  		let alert = Alert.create({
        title: 'Sorry',
        subTitle: 'You can only make withdrawals when your gift card is a multiple of 1000',
        buttons: ['OK']
      });
      this.nav.present(alert);
  	} else {
      let confirm = Alert.create({
      title: 'Withdraw GiftCard',
      message: 'Are you sure the phone number entered is correct?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.make();
          }
        }
      ]
    });
      this.nav.present(confirm);
  	}
  }
}
