import {Page, NavController,Platform,Loading} from 'ionic-angular';
import {TabsPage} from '../tabs/tabs';
import {WelcomeMessagePage} from '../welcome-message/welcome-message';
import {Facebook} from 'ionic-native';
import {LicensePage} from './../license/license';
import {DatabaseService} from '../../providers/database/database';
import {UserService} from '../../providers/user/user';
import {ServerService} from '../../providers/server/server';
import {LocalNotifications} from 'ionic-native';

@Page({
  templateUrl: 'build/pages/login/login.html',
  providers: [DatabaseService,UserService,ServerService]
})
export class LoginPage {
  static get parameters() {
    return [[NavController],[DatabaseService],[Platform],[UserService],[ServerService]];
  }

  constructor(nav,dbService,platform,userservice, serverservice) {
    this.serverservice = serverservice;
    this.data = null;
    this.dbService = dbService;
    this.userservice = userservice;
    this.platform  = platform;
    this.nav = nav;
    this.loading = Loading.create({
      spinner: 'crescent',
      content: 'Registering',
      duration: 10000,
    });
  }
  
  goTabsPage(){
    this.dbService.get('demo').then((data) => {

        var ex = JSON.parse(window.localStorage.getItem('expireDate'));

        var today = new Date();
        var expireDate = new Date(ex);

        if(ex != null && today.getTime() <= expireDate.getTime()){
          
          this.userservice.init();
          this.nav.push(TabsPage);
        } else {
          this.nav.push(LicensePage,{data: data});          
        }
    }).catch((error)=>{
      console.log(error)
    });
  }
    
    

  login(){
    Facebook.login(['email'],(response) => {
      // alert('Logged In');
      // alert(JSON.stringify(response));
      this.getDetails();
    })
  }

  getDetails(){
    Facebook.getLoginStatus((response) => {
      // console.log("facebook status");
      // console.log(JSON.stringify(response));
      if(response.status == "connected"){
        Facebook.api('/'+response.authResponse.userID+'?fields=id,name,gender,first_name,last_name,link,picture,timezone,email',['email'],(result)=>{
          // console.log("Facebook get details response"+JSON.stringify(result));
          var q = result;
          // alert(q);
          this.nav.present(this.loading);
          this.dbService.registerFacebookCallable(q,(data)=> {
            var result = data;
              this.userservice.init();
              // console.log("user service Facebook");
                // console.log(this.userservice.getData());
              this.serverservice.postFacebookAysnc(result, (err,returned_data)=>{
                // console.log("Error posting ot server"+JSON.stringify(err));
                // console.log("Data received from server"+JSON.stringify(returned_data));
                this.dbService.update(returned_data, (err,response)=>{
                  // console.log("Response from getDetails");
                  // console.log(JSON.stringify(response));
                  this.loading.dismiss();
                  this.goTabsPage(); 
                });
              });
          });          
        },
      function onError(error){
        //alert(JSON.stringify(error));
      });
      } else {
        //alert('Not loggeed in');
      }
    });
  }


  logout(){
    Facebook.logout((response) => {
      //alert(JSON.stringify(response));
    });
  }
}
