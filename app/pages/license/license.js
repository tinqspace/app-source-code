import {Page, NavController, NavParams, Loading, Alert} from 'ionic-angular';
import {TabsPage} from '../tabs/tabs';
import {WelcomeMessagePage} from '../welcome-message/welcome-message';
import {Page1} from '../page1/page1';
import {DatabaseService} from '../../providers/database/database';
import {UserService} from '../../providers/user/user';

import {Connectivity} from '../../providers/connectivity/connectivity';
import {Http,Headers} from 'angular2/http';
import 'rxjs/add/operator/map';


@Page({
  templateUrl: 'build/pages/license/license.html',
  providers: [DatabaseService,Connectivity,UserService]
})
export class LicensePage {
  static get parameters() {
    return [[NavController],[NavParams],[DatabaseService],[Http],[Connectivity],[UserService]];
  }

  onPageWillEnter(){
    this.userservice.init();
  }

  constructor(nav,params,dbService,http,connectivity, userservice) {

    this.flag = params.get('flag');
    
    console.log("params license page");
    this.data = params.get('data');
    
    if(this.data == null){
      this.data = userservice.getData();
    }

    console.log("userservice getData"+this.data);

    this.nav = nav;
    this.http = http;
    this.dbService = dbService;
    this.userService = userservice;
    this.license_key = "";
    this.connectivity = connectivity;
    this.error = "";
  }

  toTabsPage(glass_coin,gift_card,no_of_days){
  	let app_data = {
      "username": this.data.name,
      "glass_coin": glass_coin,
      "gift_card": gift_card,
      "no_of_days":no_of_days,
    };
    this.userService.init();
  	this.nav.setRoot(TabsPage);
  	this.nav.push(WelcomeMessagePage,{"data": app_data});
  }

  checkLicense(){
    console.log(this.license_key);
    if(this.license_key != ""){
      this.error = "";
      if(this.connectivity.isOnline()){
        let loading = Loading.create({
          spinner: 'crescent',
          content: 'Checking License key',
          duration: 30000,
        });
        
        this.nav.present(loading);
        
        var data ="user_id="+this.data.facebook_id+"&license_key="+this.license_key;


        let headers = new Headers();
        headers.append('Content-Type','application/x-www-form-urlencoded');
        headers.append('x-access-token','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9OnRydWUsIahT7hZtshG6kw6AW3ncuJOYw');
        this.http.post('http://admin.tinq.space/api/v1/license/use',data,{headers:headers}).map(res => res.json()).subscribe(result => {
          var q = result.data;
          console.log("result data");
          console.log(q);
          var gift_card = q.gift_card;
          var glass_coin = q.glass_coin;
          var no_of_days = q.no_of_days;
          this.dbService.license_update(glass_coin,no_of_days,gift_card,(data)=>{
            loading.dismiss(function(){});
            this.toTabsPage(glass_coin,gift_card,no_of_days);
          });  
        },error => {
              var details = error.json();
              this.error = details.message;
              loading.dismiss(function(){});
        });

      } else {
         let alert = Alert.create({
            title: 'Sorry',
            subTitle: 'There is no internet connection. Please connect to the internet',
            buttons: ['OK']
          });
          this.nav.present(alert);
      }
    } else {

      let alert = Alert.create({
        title: 'Hi there',
        subTitle: 'Please fill in the required fields.',
        buttons: ['OK']
      });
      this.nav.present(alert);
    }
    
  }

  close(){
    this.nav.pop();
  }
}
