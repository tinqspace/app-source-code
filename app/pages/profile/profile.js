import {Page, NavController, NavParams} from 'ionic-angular';
import {UserService} from '../../providers/user/user';
import {LicensePage} from '../license/license';
import {GlassCoinPage} from '../glass-coin/glass-coin';
import {GiftCardPage} from '../gift-card/gift-card';
import {TabsPage} from '../tabs/tabs';


@Page({
  templateUrl: 'build/pages/profile/profile.html',
  providers : [UserService]
})
export class ProfilePage {
  static get parameters() {
    return [[NavController],[NavParams],[UserService]];
  }

  onPageWillEnter(){
    this.userService.init();
  }

  onPageDidEnter(){
    this.userService.init();
  }
  
  constructor(nav,navparams,userService) {
    this.userService = userService;
    this.data = userService.getData();
    this.nav = nav;
    //this.userService = userService;
    this.flag = {"flag": true,"data":this.data};

    this.mydate = new Date(this.data.expire);
    console.log(this.mydate);
    this.format_date = this.mydate.toLocaleDateString('en-GB', {  
        day : 'numeric',
        month : 'short',
        year : 'numeric'
    }).split(' ').join('-');
  }
  
  goBack(){
    // this.nav.setRoot(TabsPage);
    this.nav.push(TabsPage);
  }

  license(){
    this.nav.push(LicensePage,this.flag);
  }

  coin(){
    this.nav.push(GlassCoinPage, this.flag);
  }

  giftCard(){
    this.nav.push(GiftCardPage, this.flag);
  }


}
