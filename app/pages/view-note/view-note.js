import {Page, NavController,NavParams,Modal} from 'ionic-angular';
import {TabsPage} from '../tabs/tabs';
import {UserService} from '../../providers/user/user';
import {GamePage} from '../game/game';
import {SuccessCreateNote} from '../modal/success-create-note/success-create-note';
import {SelectNoteModal} from '../modal/select-note/select-note';

import {DatabaseService} from '../../providers/database/database';

@Page({
  templateUrl: 'build/pages/view-note/view-note.html',
  providers: [UserService]
})
export class ViewNotePage {
  static get parameters() {
    return [[NavController],[NavParams],[UserService],[DatabaseService]];
  }

  onPageWillEnter(){
    console.log("Entered view note");
    this.userservice.init();
  }
  
  constructor(nav,params,userservice,dbService) {
    this.nav = nav;
    this.note = params.get('note');
    this.view = params.get('view') || false;
    this.note_index = params.get('note_index') || false;

    this.userservice = userservice;
    this.dbService = dbService;
    this.data = userservice.getData();

    this.current = 1;
    this.index = 0;
    this.facts = this.note.facts[this.index];
    console.log("this.facts");
    console.log(this.facts);
    this.total = this.note.facts.length;
    this.start = false;
    this.finished = false;
  }
  
  deleteNote(){
      var current_note = this.note;
      var index = this.data.notes.findIndex(x => x.topic == current_note.topic && x.title == current_note.title);
      console.log(index)
      if(index < 0) return null;
      this.data.notes.splice(index,1);
      this.dbService.save(this.data);
      this.userservice.init();
      // returns the note position
      return index;
  }

  editNote(){
    console.log("Edit note");
    var index = this.deleteNote();
    let modal = Modal.create(SuccessCreateNote, {note: this.note, edit : true, position: index} );
    this.nav.present(modal);
    //this.nav.dismiss();

    //l/et modal = 
  }

  random(){
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var text = "";
      for( var i=0; i < 4; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
  }

  clickNote(){
    //console.log(i);
    //var note = this.notes[i];
    var data = this.userservice.getData();
    if(this.view === false){
      var note_index = data.notes.length - 1;
      var noteModal = Modal.create(SelectNoteModal , {"note_index": note_index,"data": data})
    } else {
      var noteModal = Modal.create(SelectNoteModal , {"note_index": this.note_index,"data": data})
    }
    this.nav.present(noteModal);
  }

  startGame(){
    var data = this.userservice.getData();
    if(this.view === false){
      var note_index = data.notes.length - 1;
      this.nav.push(GamePage,{data: data,note_index : note_index, "id": this.random()}); 
    } else {
      this.nav.push(GamePage,{data: data,note_index : this.note_index, "id": this.random()}); 
    }
  }

  later(){
    //this.nav.setRoot(TabsPage);
    this.nav.push(TabsPage);
  }

  addNext(){
    this.index++;
    if(this.index <= (this.note.facts.length - 1)){
        this.start = true;
      if(this.index == (this.note.facts.length - 1)){
        this.finished = true;
      }
      this.facts = this.note.facts[this.index];
    }
  }

  prev(){
    this.index--;
    if(this.index >= 0){
      this.finished = false;
      if(this.index == 0){
        this.start = false;
      }
      this.facts = this.note.facts[this.index];
    }
  }

  save(){

  }



}
