import {Page,NavController,Platform,NavParams,Modal} from 'ionic-angular';
import {CreateNotePage} from '../create-note/create-note';
import {DatabaseService} from '../../providers/database/database';
import {UserService} from '../../providers/user/user';
import {SelectNoteModal} from '../modal/select-note/select-note';


@Page({
  templateUrl: 'build/pages/page1/page1.html',
  providers: [DatabaseService,UserService]
})

export class Page1 {
  
  static get parameters(){
  	return [[NavController],[DatabaseService],[Platform],[NavParams],[UserService]];
  }

 constructor(nav,dbService,platform,navparams,userservice) {
  
    this.platform = platform;

    this.data = JSON.parse(window.localStorage.getItem("userdata"));
    console.log("this.data")
    //console.log(this.data);
    //console.log(userservice.data());
    this.images = ["pending.png","1preschooler.png","2middleschooler.png",
    "3junior.png","4senior.png","5freshman.png","6sophomore.png","7graduate.png",
    "8masters.png","9doctor.png","10prof.png"];
    
    // console.log(this.data.notes);

    this.notes = this.data.notes.filter(function(item){
      return (item.status == 1);
    });
    
    // console.log(this.notes);
   
    this.colors = ['#727272','#ffa000','#9c27b0','#03a9f4','#E91e63'];
  	this.nav = nav;
    this.dbService = dbService;
    this.color = this.colors[this.seedNumber()];

  }

  getColor(){
    console.log("1")
    console.log(this.random());
    return this.colors[this.random()];
  }

  random(){
    return Math.floor(Math.random() * 4);
  }
  
  createNote(){
  	this.nav.push(CreateNotePage);
  }

  clickNote(i){
    console.log(i);
    var note = this.notes[i];
    var title = note.title;
    var index = this.data.notes.findIndex(x => (x.title == title) && (x.status == 1));
    var noteModal = Modal.create(SelectNoteModal , {"note_index": index,"data":this.data})
    this.nav.present(noteModal);
  }


  seedNumber(){
    return Math.floor((Math.random * 4 ) + 0);
  }
}
