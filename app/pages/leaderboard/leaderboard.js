import {Page, NavController} from 'ionic-angular';

@Page({
  templateUrl: 'build/pages/leaderboard/leaderboard.html',
})
export class LeaderboardPage {
  static get parameters() {
    return [[NavController]];
  }

  constructor(nav) {
    this.nav = nav;
  }
}
