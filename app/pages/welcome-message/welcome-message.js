import {Page, NavController,NavParams} from 'ionic-angular';
import {UserService} from '../../providers/user/user';
import {TabsPage} from '../tabs/tabs';
@Page({
  templateUrl: 'build/pages/welcome-message/welcome-message.html',
  providers: [UserService]
})
export class WelcomeMessagePage {
  static get parameters() {
    return [[NavController],[UserService],[NavParams]];
  }

  constructor(nav, userService,navParams) {
    this.nav = nav;
    this.userService = userService;
    this.navParams = navParams;
    this.data = this.navParams.get('data');
  }

  continue(){
    // console.log("Continue");
   // alert("hello");
    //console.log(this.data);
    //var myData = this.userService.getData();
    //this.nav.pop();
    this.nav.setRoot(TabsPage);
    this.nav.push(TabsPage);
  }
}
