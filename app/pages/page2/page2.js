import {Page,NavController,Modal} from 'ionic-angular';
import {UserService} from '../../providers/user/user';
import {DatabaseService} from '../../providers/database/database';

import {ContinueNoteModal} from '../modal/continue-note/continue-note';

@Page({
  templateUrl: 'build/pages/page2/page2.html',
  providers : [UserService,DatabaseService]
})
export class Page2 {
  static get parameters(){
  	return [[NavController],[UserService],[DatabaseService]];
  }

  constructor(nav,userService, dbService) {
    this.nav = nav;
    this.data = userService.getData();
    this.userService = userService;
    this.dbService = dbService;
    this.page2_notes = this.data.notes.filter(function(item){
      return (item.status == 0);
    });

    this.pending = this.page2_notes.length;

    console.log("data from service 2");
    //console.log(this.notes);
  }

  clickNote(i){
    var current_note = this.page2_notes[i];
    var index = this.data.notes.findIndex(x => x.topic == current_note.topic);
    console.log(index)
    this.data.notes.splice(index,1);
    this.dbService.save(this.data);
    this.userService.init();
    //console.log(JSON.stringify(this.data));

    let modal = Modal.create(ContinueNoteModal,{note: current_note});
    this.nav.present(modal);
  }
}
