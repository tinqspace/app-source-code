import {Page,NavController,NavParams,Platform, Loading} from 'ionic-angular';
import {Page1} from '../page1/page1';
import {Page2} from '../page2/page2';
import {Page3} from '../page3/page3';
import {ProfilePage} from '../profile/profile';
import {SettingsPage} from '../settings/settings';
import {UserService} from '../../providers/user/user';

@Page({
  templateUrl: 'build/pages/tabs/tabs.html',
  providers : [UserService]
})

export class TabsPage {
  static get parameters() {
    return [[Platform],[NavController],[NavParams],[UserService]];
  }

  onPageWillEnter(){
    this.userService.init();
  }

  onPageDidEnter(){
    var data = this.userService.getData()
    var notes = data.notes;
    var pending = notes.filter(function(item){
      return (item.status == 0);
    });

    this.pendings = pending.length;
    // console.log("will enter");
  }

  constructor(platform,nav,navparams,userService) {
    this.tab1Root = Page1;
    this.tab2Root = Page2;
    this.tab3Root = Page3;

    this.nav = nav;
    this.platform = platform;
    
    this.userService = userService;
  }

  profilePage(){
  	this.nav.push(ProfilePage);
  }
  
}
