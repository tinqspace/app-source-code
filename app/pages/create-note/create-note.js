import {Platform, Page, NavController, Modal,Alert} from 'ionic-angular';
import {SuccessCreateNote} from '../modal/success-create-note/success-create-note';
import {Component} from 'angular2/core';
import {FORM_DIRECTIVES,FormBuilder,ControlGroup,Validators,AbstractControl} from 'angular2/common';
import {Database} from '../../provider/database';

@Page({
  templateUrl: 'build/pages/create-note/create-note.html',
  directives : [FORM_DIRECTIVES]
})

export class CreateNotePage {
  static get parameters() {
    return [[NavController],[FormBuilder],[Platform]];
  }

  //page life cycle hook
  onPageDidEnter(){
    var data = this.db.get('demo');
    console.log(data);
  }

  constructor(nav,fb,platform) {

    this.authForm =  ControlGroup;

    this.title =  AbstractControl;
    this.topic =  AbstractControl;
    this.material = AbstractControl;
    this.author = AbstractControl;
    this.school = AbstractControl;
    this.days = AbstractControl;

    this.colors = ['#388E3C','#ffa000','#9c27b0','#03a9f4','#E91e63','#303F9F','#512DA8','#E64A19'];
    this.authForm = fb.group({
      'title' : ['',Validators.compose([Validators.required,Validators.minLength(3)])],
      'topic' : ['',Validators.compose([Validators.required,Validators.minLength(3)])],
      'material': ['',Validators.compose([Validators.required])],
      'school': ['',Validators.compose([Validators.minLength(0)])],
      'author': ['',Validators.compose([Validators.minLength(0)])],
      'days' : ['',Validators.compose([])]
    });

    this.title = this.authForm.controls['title'];
    this.topic = this.authForm.controls['topic'];
    this.material = this.authForm.controls['material'];
    this.school = this.authForm.controls['school'];
    this.author = this.authForm.controls['author'];
    this.days  = this.authForm.controls['days'];

    this.selectedMaterial = "";

    this.making_change = true;

    this.nav = nav;

    this.db = new Database();

    this.platform = platform;
    // platform.ready().then(() => {
    //   document.addEventListener('backbutton', () => {
    //     console.log("working2backbutton");
    //     if (!this.nav.canGoBack()) {
    //       this.platform.exitApp()
    //       return;
    //     }
    //     this.nav.pop()
    //   });
    // });
  }

  seedNumber(){
    return Math.floor((Math.random() * 8 ) + 0);
  }

  onSubmit(data){
    if(this.authForm.valid){
      console.log(data);
      this.createNote(data);
    } else  {
      let  alert = Alert.create({ 
      title: 'Hey there!', 
      subTitle: 'Please all fields are required to be properly filled.', 
      buttons: ['Ok'] 
    }); 
     this.nav.present(alert);
    }
  }

  onChange(){
    this.making_change = false;
  }

  goBack(){
    this.nav.pop();
  }
  
  createNote(data){
    console.log("hello");
    data.status = 0;
    data.state = 'ideas';
    data.level = 0;
    data.played = 0;
    data.created_at = new Date();
    var index = this.seedNumber();
    data.color = this.colors[index];
    let s = Modal.create(SuccessCreateNote,{note : data});
    this.nav.present(s);
  }
}
