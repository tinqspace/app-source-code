import {Page, NavController, Alert, Loading,NavParams} from 'ionic-angular';
import {ServerService} from '../../providers/server/server';
import {UserService} from '../../providers/user/user';

@Page({
  templateUrl: 'build/pages/glass-coin/glass-coin.html',
  providers: [ServerService,UserService]
})

export class GlassCoinPage {
  static get parameters() {
    return [[NavController],[ServerService],[NavParams],[UserService]];
  }

  onPageWillEnter(){
    this.userservice.init();
  }

  onPageWillLeave(){
    this.userservice.init();
  }

  constructor(nav,server,params,userservice) {
    this.nav = nav;
    this.server = server;
    this.userservice = userservice;
    this.key = "";
    this.data = this.userservice.getData();
    this.email = this.data.email;
    this.message = "";
  }

  purchase(){
  	if(this.key != ""){
  		let loading = Loading.create({
          spinner: 'crescent',
          content: 'Checking Coin key',
          duration: 10000,
        });
        
        this.nav.present(loading);

        var data = "key="+this.key+"&user_id="+this.email;

        this.server.post('coin/use',data,(error,response) => {
        	if(error){
        		this.message = error.message;
        		loading.dismiss(function(){});
        		return;
        	} else {
        		this.data.glass_coins += response.data.amount;
        		this.userservice.save(this.data);
        		// console.log("coins/use");
        		// console.log(this.userservice.getData());
            this.userservice.init();
            
        		this.message = "You have successfully bought "+response.data.amount+" coins";
        		loading.dismiss();
        	}
        });
  	} else {
      let alert = Alert.create({
        title: 'Hi there',
        subTitle: 'Please fill in the required fields.',
        buttons: ['OK']
      });
      this.nav.present(alert);
  	}
  }

}
