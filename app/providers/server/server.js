import {Injectable} from 'angular2/core';
import {Http,Headers} from 'angular2/http';
import {Observable} from 'rxjs/Observable';



@Injectable()
export class ServerService {
	
	static get parameters(){
		return [[Http]];
	}

	constructor(http){
		this.BASE_URL = 'http://admin.tinq.space/api/v1/';
		this.http = http;
	}

	getSync(uri,callable){
		let headers = new Headers();
        headers.append('x-access-token','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9OnRydWUsIahT7hZtshG6kw6AW3ncuJOYw');
		this.http.get(this.BASE_URL+uri,{headers:headers}).map(res => res.json()).subscribe(data =>{
			callable(data);
		}, error => {
			console.log(error);
		});
	}

	post(uri, data,callable){
		let headers = new Headers();
        headers.append('Content-Type','application/x-www-form-urlencoded');
        headers.append('x-access-token','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9OnRydWUsIahT7hZtshG6kw6AW3ncuJOYw');
        this.http.post(this.BASE_URL+uri,data,{headers:headers}).map(res => res.json()).subscribe(data => {
          callable(null,data);
        },error => {
          callable(error,null);
        });
	}
	

	transferPoints(email,data,callable){
		if(data == "" || data == null){
			return;
		} else {
			var formdata = "email="+email+"&points="+data;
			this.post('leaderboard/',formdata, function(err, data){
				if(err){
					// console.log("err"+JSON.stringify(err));
				} else {
					callable();
					// console.log("data"+JSON.stringify(data));
				}
			});
		}
	}

	transferCoin(email, coin ,callable){
		var formdata = "email="+email+"&amount="+coin;
		this.post('users/coin/',formdata,(err,data)=>{
			console.log("err");
			callable();
		});
	}


	postFacebookAysnc(data,callable){
		var formdata = "email="+data.email+"&facebook_id="+data.facebook_id+"&name="+data.name+"&first_name="+data.first_name+"&last_name="+data.last_name+"&gender="+data.gender+"&picture="+data.picture+"&link="+data.link+"&locale="+data.locale+"&gift_card="+data.gift_card+"&glass_coins="+data.glass_coins;
		// console.log("form data for server from facebook :::) "+ formdata);
		// console.log("making facebook post request to server");
		this.post('users/',formdata,callable);
		// console.log("making facebook create request server");
	}

	sendNote(note,data,callable){
		var email = data.email;
		var n = {
			"note" : note
		};
		var formdata = "email="+email+"&note="+JSON.stringify(n);
		this.post('users/note/',formdata,(err, data)=>{
			// console.log("send note"+JSON.stringify(err));
			// console.log("send note"+JSON.stringify(data));
			callable();
		});
	}

	getGiftCard(facebook_id,callable){
		this.getSync('users/'+facebook_id,(data)=>{
			callable(data);
		});
	}

	


}