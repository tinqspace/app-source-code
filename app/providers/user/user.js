import {Injectable, Inject} from 'angular2/core';
import {Http} from 'angular2/http';
import {DatabaseService} from '../database/database';

@Injectable()
export class UserService {
	static get parameters(){
  		return [DatabaseService];
  	}
	constructor(dbService){
		console.log("user service");
		this.dbService = dbService;
		this.data = "";
		console.log("user service2");

	}
	
	getData(){
		this.data = JSON.parse(window.localStorage.getItem("userdata"));
		return this.data;
	}

	updatePointStatus(points){
		// assumption Matde here is that if there is an addition to points it will be pushed to the end of the array

		var current_point = window.localStorage.getItem('userpoints');
		var points_length = points.length;

		for(i = 0; i < points.length; i++){
			points.status = 1;
		}
		window.localStorage.setItem('userpoints',points);
	}

	resetPoint(){
        
		window.localStorage.setItem('userpoints',"");
	}
	
	getPoint(){
		var points = window.localStorage.getItem('userpoints');
		return points;
	}

	points(points){
		var mydata = {
			points: points,
			created_at: new Date(),
			status : 0,
		};

		var current = window.localStorage.getItem('userpoints');
		console.log("current sgas");
		console.log(current);
		if(current == null || current == ""){
			var data = [{
			points: points,
			created_at: new Date(),
			status : 0,
			}];
		} else {
			var data = JSON.parse(current);
			data.push(mydata);
		}

		window.localStorage.setItem('userpoints',JSON.stringify(data));
	}
	
	totalPoint(created_at){

		var myPoints = 0;
		var userpoints = this.getPoint();
		if(userpoints == "" || userpoints == null || userpoints == undefined){
			return;
		}

		var points = JSON.parse(userpoints);

		if(points == null || points == undefined || points == ""){
			return 0;
		}

        for(var i = 0; i < points.length; i++){
            var current_point = points[i];
            console.log("Current point json"+JSON.stringify(current_point));
            var current_date_point = new Date(current_point.created_at);
            var goalDate = new Date(created_at);
            if(current_date_point >= goalDate){
                console.log("current points"+current_point.points);
                myPoints += parseInt(current_point.points);
            }
        }

		return myPoints;
	}



	init(){
		this.dbService.get('demo').then((data) => {
			window.localStorage.setItem("userdata",JSON.stringify(data));
          // console.log(data);
          return data;
       }).catch((error)=>{
         // console.log(error)
       	 return error;
      });
	}

	initCallable(){

	}

	save(data){
		this.dbService.save(data);
		this.init();
	}

	saveCallable(data,callable){
		this.dbService.saveCallable(data,function(err,res){
			this.init();
			callable(err,res);
		});
	}

	setLevel(level,note_index,callable){
		this.dbService.get('demo').then((data)=>{
			data._id = 'demo';
			data.notes[note_index].level = level;
			this.save(data);
			callable();
		}).catch((error)=> {	
			console.log(error);
		});
	}

	renew(level,note_index,glass_coin, callable){
		this.setLevel(level,note_index,()=>{
			this.removeCoin(glass_coin);
			this.init();
			callable();
		});
	}

	removeCoin(glass_coin){
		this.dbService.get('demo').then((userdata)=>{
			var data = userdata;
			//data.
			data.glass_coins -= glass_coin;
			this.save(data);
		}).catch((error)=>{
			
		})
	}

	getCoin(callable){
		this.dbService.get('demo').then((userdata)=>{
			var coin = userdata.glass_coins;
			callable(coin);
		}).catch((error)=>{

		});
	}

	notify(callable){
		this.dbService.get('demo').then((data)=>{
			var alerts = [];
			var notes = data.notes.filter(function(item){
      			return (item.status == 1);
    		});
    		
			var notes_length = notes.length;
			for(var i =0; i < notes_length; i++){
				var note = notes[i];
				var days = parseInt(note.days);
				//var number_of_facts = note.facts.length;
				//no of hourse
				var no_of_hours = 24 * days;
				var level = note.level;
				if( level == 9){
					continue;
				}
				if(!note.created_at){
					continue;
				}
				var hour_per_level = parseInt(no_of_hours / 10);
				var created_at = new Date(note.created_at);
				console.log("Creatted at");
				console.log(created_at);
				
				var next_time = created_at.setDate(created_at.getDate() + (level * hour_per_level));
				var title = note.title;

				var alert = {
					title : title,
					next_time: next_time,
					interval: hour_per_level * 60, //coverting to minutes
				};
				alerts.push(alert);
			}
			callable(alerts);
		});
	}

	deduct(callable){
		// this.dbService.get('demo').then((data)=>{
		// 	var alerts = [];
		// 	var notes = data.notes;
		// 	var notes_length = notes.length;
		// 	for(var i =0; i < notes_length; i++){
		// 		var note = notes[i];
		// 		var days = parseInt(note.days);
		// 		//var number_of_facts = note.facts.length;
		// 		//no of hourse
		// 		var no_of_hours = 24 * days;
		// 		var level = note.level;
		// 		if( level == 9){
		// 			continue;
		// 		}
		// 		var hour_per_level = parseInt(no_of_hours / 10);
		// 		var created_at = note.created_at;
		// 		var next_time = created_at.setDate(created_at.getDate + (level * hour_per_level));
				

		// 		var title = note.title;

		// 		var alert = {
		// 			title : title,
		// 		};

		// 		alerts.push(alert);
		// 	}
		// 	callable(alerts);

		// })
	}

	diffInDays(date,date2){
		// var timeDiff = Math.abs(date2.getTime() - date.getTime());
		// var diffInhour = Math.ceil(timeDiff / (1000 * 3600));
		// return diffInhour;
	}





}