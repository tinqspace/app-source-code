import {Injectable} from 'angular2/core';

@Injectable()
export class DatabaseService {
	static get parameters(){
		return []
	}

	constructor(){
		this.database = new PouchDB("glass");
		// console.log("PouchDB getInstance");
		// console.log(this.database);
	}

	get(documentId){
		return this.database.get(documentId);
	}

	getInstance(documentId){
		this.database.get(documentId).then((data)=>{
			return data;
		}).catch((error)=>{
			// console.log(error);
		});
	}

	registerFacebook(data){
 		console.log("register facebook");
		this.database.get('demo').then((response)=>{
			// console.log("register facebook response");
			// console.log("demo registerFacebook response"+JSON.stringify(response));
			response.facebook_id = data.id;
			response.name = data.name;
			response.first_name = data.first_name;
			response.last_name = data.last_name;
			response.link = data.link;
			response.picture  = data.picture.data.url;
			response.locale = data.timezone;
			response.email = data.email;
			response.gender = data.gender;
			this.save(response);
          	window.localStorage.setItem('userpoints','');
          	window.localStorage.setItem('userdata',response);
		}).catch((error)=>{
			// console.log("error register facebook");
			// console.log(JSON.stringify(error));
		});
	}



	registerFacebookCallable(data,callable){
 		console.log("register facebook");
		this.database.get('demo').then((response)=>{
			// console.log("register facebook response");
			// console.log("registerFacebookCallable respons"+JSON.stringify(response));

			response._id = "demo";
			response.facebook_id = data.id;
			response.name = data.name;
			response.first_name = data.first_name;
			response.last_name = data.last_name;
			response.link = data.link;
			response.picture  = data.picture.data.url;
			response.locale = data.timezone;
			response.email = data.email;
			response.gender = data.gender;
			// console.log("JSON document to be saevd registerFacebookCallable"+JSON.stringify(response));
			this.save(response);
          	window.localStorage.setItem('registered','true');
          	window.localStorage.setItem('userdata',JSON.stringify(response));
          	var s = window.localStorage.getItem('userdata');
          	// console.log("checking response...."+JSON.stringify(response));
          	callable(response);
		}).catch((error)=>{
			// console.log("error register facebook");
			// console.log(JSON.stringify(error));
		});
	}

	

	initDB(){
		var document = {
			_id : "demo",
			name : "",
			user_id : "",
			first_name : "",
			last_name : "",
			picture: "",
			facebook_id : "",
			registered : 0,
			email : "",
			timezone : "",
			gift_card: 0,
			status : "1", //pending or active
			notes : [],
			created_at : new Date().toISOString(),
			glass_coins : 0,
			glass_points: 0,
			expire : "",
			gender : "",
			license_key:""
		};

		this.save(document);
	}

	initPoints(){
		var points = {
			_id : "userpoints",
			
		}
	}


	addPoints(){
		this.database.get('userpoints').then((response)=>{

		});
	}


	
	save(document){
		//console.log("document"+document);
		if(!document._id){
			this.database.post(document).then(function(response) {

				//console.log(response);
				//window.localStorage.setItem("userdata",JSON.stringify(response));
			}).catch(function(error) {

				console.log(JSON.stringify(error));
			})
		} else {
			this.database.put(document).then(function(response) {
				//console.log("document passed into put"+JSON.stringify(document));
				//console.log("response from put"+JSON.stringify(response));

				// //window.localStorage.setItem("userdata",JSON.stringify(response));
				//console.log(response);
			}).catch(function(error){
				// console.log(error);
				console.log("respnse from put error"+JSON.stringify(error));
			})
		}
	}

	saveCallable(document, callable){
		if(!document._id){
			this.database.post(document).then(function(response) {
				callable(null,response);
			}).catch(function(error) {
				callable(error,null);
			});
		} else {
			this.database.put(document).then(function(response) {
				callable(null,response)
			}).catch(function(error){
				callable(error,null);
			});
		}
	}

	license_update(glass_coins,days,gift_card,callable){

		this.database.get('demo').then((userdata)=>{
			console.log(userdata);
			var data = userdata;
			data._id = "demo";
			data.glass_coins += glass_coins;

			var d = JSON.parse(window.localStorage.getItem('expireDate'));
			console.log("d"+d);

			var current_date;
			if(d != null){
				current_date = new Date(d);
			} else {
				current_date = new Date();
			}

			current_date.setDate(current_date.getDate() + days);
			data.expire = current_date;

			console.log(JSON.stringify(current_date)+"current_date");
			window.localStorage.setItem('expireDate',JSON.stringify(current_date));
			data.gift_card += gift_card;
			this.saveCallable(data,callable);
			// console.log("Expired Date "+window.localStorage.getItem('expireDate'));
			
			}).catch((error)=>{
				// console.log(JSON.stringify(error));
			});
		
	}

	updateNote(){

	}

	addCoin(glass_coin){
		this.database.get('demo').then((userdata)=>{
			var data = userdata;
			data._id = 'demo';
			data.glass_coins += glass_coin;
			this.save(data);
		}).catch((error)=>{
			
		})
	}

	addGiftCard(gift_card){
		this.database.get('demo').then((data)=>{
			data._id = 'demo';
			data.gift_card = parseInt(gift_card);
			this.save(data);
		}).catch((error)=>{
			// console.log("Adding gift card"+JSON.stringify(error));
		});
	}

	deductGiftCard(gift_card){
		this.database.get('demo').then((data)=>{
			data._id ='demo';
			data.gift_card -= parseInt(gift_card);
			this.save(data);
		}).catch((error)=>{
			// console.log("Error Deducting Gift Card"+ JSON.stringify(error));
		});
	}

	update(returned_data, callable){
		this.database.get('demo').then((userdata)=>{
			userdata._id = "demo";
			userdata.gift_card += parseInt(returned_data.data.gift_card);
			userdata.glass_coins += parseInt(returned_data.data.glass_coins);

			if(returned_data.data.license){
				var today = new Date();
		        var expireDate = new Date(returned_data.data.license);
		        
		        // console.log("Today"+today);
		        // console.log("Expired"+expireDate);

		        if(today.getTime() <= expireDate.getTime()){
					userdata.expire = new Date(returned_data.data.license);
					window.localStorage.setItem('expireDate',JSON.stringify(expireDate));
		        }
			}

			var mynotes = returned_data.data.notes;			
			if(mynotes.length > 0){
				for(var i = 0; i < mynotes.length; i++){
					userdata.notes.push(mynotes[i]);									
				}
			}
			this.saveCallable(userdata, callable);
		});
	}
}