import 'es6-shim';
import {App, Platform,IonicApp} from 'ionic-angular';
import {StatusBar, LocalNotifications} from 'ionic-native';
import {LoginPage} from './pages/login/login';
import {CreateNotePage} from './pages/create-note/create-note';
import {TabsPage} from './pages/tabs/tabs';
import {DatabaseService} from './providers/database/database';
import {Connectivity} from './providers/connectivity/connectivity';
//remove
import {UserService} from './providers/user/user';
import {ServerService} from './providers/server/server';

import {GamePage} from './pages/game/game';
import {IdeaGamePage} from './pages/idea-game/idea-game';
import {FactGamePage} from './pages/fact-game/fact-game';
import {LicensePage} from './pages/license/license';

@App({
  template: '<ion-nav [root]="rootPage"></ion-nav>',
  config: {
    scrollAssist: false,    // Valid options appear to be [true, false]
    autoFocusAssist: false  // Valid options appear to be ['instant', 'delay', false]
  }, // http://ionicframework.com/docs/v2/api/config/Config/
  providers:[DatabaseService,UserService,Connectivity,ServerService]
})

export class MyApp {
  
  static get parameters() {
    return [[Platform],[DatabaseService],[UserService],[Connectivity],[ServerService],[IonicApp]];
  }

  registerBackButtonListener() {
        document.addEventListener('backbutton', () => {
            var nav = this.getNav();
            if (nav.canGoBack()) {
            nav.pop();
            }
            else {
                navigator.app.exitApp();
            }
        });
    }

  constructor(platform,dbService,userService,connectivity,server,ionicapp) {

    this.dbService = dbService;
    this.userservice = userService;
    this.connectivity = connectivity;
    this.server = server;
    //this.nav = nav;
    this.platform = platform;
    this.app = ionicapp;
    var applauchCount = window.localStorage.getItem('lauchCount');
    var registered    = window.localStorage.getItem('registered');
    // this.rootPage = TabsPage;


    if(registered == "true"){

      var ex = JSON.parse(window.localStorage.getItem('expireDate'));

      if(ex != "" || ex != null || ex != undefined){
        var today = new Date();
        var expireDate = new Date(ex);
           if(today.getTime() <= expireDate.getTime()){
              this.rootPage = TabsPage;
           } else {
              window.IonicNative.LocalNotifications.schedule({
                id: 1,
                text: "Your Glass License has Expired",
                title : "Expired License",
                sound: 'file://sound.mp3',
                data: {"hello" : "hi"}
              });

              this.rootPage = LicensePage;
           }
        } else {
          this.rootPage = LicensePage;
        }
      } else {
        if(applauchCount == "1"){
          this.userservice.init();
          this.rootPage = LoginPage;
        } else {
          this.dbService.initDB();
          this.rootPage = LoginPage;
          window.localStorage.setItem('lauchCount','1');
        }
      }
    
    platform.ready().then(() => {

      StatusBar.overlaysWebView(true);
      StatusBar.backgroundColorByHexString("#303F9F");

      if(this.connectivity.isOnline() && applauchCount == "1"){
        new Promise(resolve=>{
            this.userservice.notify((data)=>{
                var array = [];
                for(var i = 0; i < data.length; i++){
                    array.push({
                      id: i,
                      text: "Time is due for this note" ,
                      title :data[i].title ,
                      at: data[i].next_time,
                      every: data[i].interval,
                      icon: 'file://build/images/icon.png',
                    });
                }
                window.IonicNative.LocalNotifications.schedule(array);
            });
        }).catch((error)=>{
          console.log(error);
        });

        new Promise(resolve => {
                // console.log("Promise resolving for isOnline");
                // console.log()
                var mydata = this.userservice.getData();
                var notes = mydata.notes;
                var coin  = mydata.glass_coins;
                var email = mydata.email;
                var facebook_id = mydata.facebook_id;
                var point = this.userservice.getPoint();
                this.server.sendNote(notes, mydata,()=>{
                   this.server.transferPoints(email,point,()=>{
                        this.userservice.resetPoint();
                        this.server.transferCoin(email, coin ,(err,mycoin_data)=>{
                            //console.log(mycoin_data);
                            this.server.getGiftCard(facebook_id,(data)=>{
                                console.log("get gift gift_card");
                                console.log(data);
                                // console.log(data.data[0].gift_card);
                               this.dbService.addGiftCard(data.data[0].gift_card);
                           });
                        });
                   });
                   console.log("Done with promise resolving");
                 });
            }
            ,reject => {
                 console.log("an error occurred")
        });
     }
     
    });
  }
}